$here = Split-Path -Parent $MyInvocation.MyCommand.Path
[io.fileinfo]$fileToTest = "$here\$($MyInvocation.MyCommand.Name.Replace(`"Tests.`",`"`"))"

# set-psdebug -trace 1;set-psbreakpoint -variable boom

# $boom = 0
#import-module (resolve-path "$(([io.directoryinfo]$here).parent.fullname)\*.psm1")

if (-not (test-path $fileToTest)){ write-error "Unable to find the file $fileToTest" }

. "$here\Invoke-CustomZip.ps1"
. $fileToTest

$commandletName = $fileToTest.BaseName

Describe -Tags "Example" "$commandletName" {

    It "should be visible on the filesystem" { test-path $fileToTest | Should Be True }

    It "should be accessible by Get-Command" { (get-command $commandletName).count | should not be 0 }

    It "should have a valid alias named rolliislogs" { (Get-Alias rolliislogs).Name.length | should not be 0 }

    It "should have a valid alias named backupiislogs" { (Get-Alias backupiislogs).Name.length | should not be 0 }

    It "should have a valid alias named deleteiislogs" { (Get-Alias deleteiislogs).Name.length | should not be 0 }

    It "should be able to access a local iis instance" { 
        if(((Get-Module -ListAvailable webadministration).Count -gt 0) -and (test-path "C:\inetpub\logs\LogFiles")){
            import-module webadministration
            (backupiislogs -source 'IIS:\Sites' -destination "c:\temp\backup" -overwrite).count | should BeGreaterThan 0 
            rm -rec -for "c:\temp\backup\*"
            (backupiislogs "c:\temp\backup" -overwrite).count | should BeGreaterThan 0 
            (deleteiislogs) | should not be false
        }   
    }
}