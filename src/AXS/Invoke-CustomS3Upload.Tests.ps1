$here = Split-Path -Parent $MyInvocation.MyCommand.Path

[io.fileinfo]$fileToTest = "$here\$($MyInvocation.MyCommand.Name.Replace(`"Tests.`",`"`"))"

if (-not (test-path $fileToTest)){ write-error "Unable to find the file $fileToTest" }

. $fileToTest

$commandletName = $fileToTest.BaseName

Describe -Tags "$commandletName" "Test" {

    It "should be visible on the filesystem as $fileToTest" { test-path $fileToTest | Should Be True }

    It "should be accessible by Get-Command as $commandletName" { (get-command $commandletName).count | should not be 0 }

    It "should have a valid alias named UploadTos3" { (Get-Alias UploadTos3).Name.length | should not be 0 }

}
