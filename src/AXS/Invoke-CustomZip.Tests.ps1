$here = Split-Path -Parent $MyInvocation.MyCommand.Path
[io.fileinfo]$fileToTest = "$here\$($MyInvocation.MyCommand.Name.Replace(`"Tests.`",`"`"))"

if (-not (test-path $fileToTest)){ write-error "Unable to find the file $fileToTest" }

. $fileToTest
$commandletName = $fileToTest.BaseName
$t = "$env:temp\"
$one = "$t\_1"
$two = "$t\_2"
$three = "$t\_3"
$zipexe = "$here\..\..\packages\7-zip.CommandLine\tools\7za.exe"
Describe -Tags "Example" "Zip functions" {

    It "should be visible on the filesystem" { test-path $fileToTest | Should Be True }

    It "should be accessible by Get-Command" { (get-command $commandletName).count | should not be 0 }

    It "should have a valid alias named zip" { (Get-Alias zip).Name.length | should not be 0 }

    It "should be able to find 7zip executable" { test-path $zipexe | should be $true }

    It "should be able to make a test directory in `$t" {
        "$one" | %{if( -not (test-path $_)){mkdir $_}}
        "$two" | %{if( -not (test-path $_)){mkdir $_}}
        "$three" | %{if( -not (test-path $_)){mkdir $_}}
        test-path "$one" | should be $true
        test-path "$two" | should be $true
        test-path "$three" | should be $true
        echo "testfile" > "$one\test.txt" ; test-path "$one\test.txt" | should be $true
        echo "testfile" > "$two\test.txt" ; test-path "$two\test.txt" | should be $true
        echo "testfile" > "$three\test.txt" ; test-path "$three\test.txt" | should be $true
    }

    It "should be able to zip an individual file" {
        zip "$one\test.txt" "$two\one.zip" -overwrite -zipexe $zipexe | should be $true
    }
    
    It "should zip an individual folder with a zip destination specified" {
        zip "$two\" "$three\two.zip" -overwrite -zipexe $zipexe | should be $true
    }

    It "should zip an individual file with only destination folder specified, not a destination file" {
        zip "$two\test.txt" "$three\" -overwrite -zipexe $zipexe | should be $true
    }

    It "should zip an individual folder with a destination folder specified, not a destination file" {
        zip "$three\" "$one\" -overwrite -zipexe $zipexe| should be $true
    }

    It "should zip an individual file with 7zip and with zip destination specified" { 
        zip "$three\test.txt" "$two\_3.zip" -overwrite -zipexe $zipexe | should be $true
    }

    It "should zip an individual file with 7zip and with destination folder specified, not zip file" { 
        test-path "$two\_3.zip" -ea SilentlyContinue | should be $true
        zip "$two\_3.zip" "$three\" -overwrite -zipexe $zipexe | should be $true
    }

}

Describe -Tags "Example" "Unzip Functions" {

    It "should be visible on the filesystem" { test-path $fileToTest | Should Be True }

    It "should be accessible by Get-Command" { (get-command $commandletName).count | should not be 0 }

    It "should have a valid alias named unzip" { (Get-Alias unzip).Name.length | should not be 0 }

    It "should fail to unzip a directory as the 1st argument must be a zip file" {
        unzip "$two\" "$temp\" -overwrite -zipexe $zipexe| should be $false
    }

    It "should fail to unzip a file that isnt a zip file." { 
        unzip "$three\test.txt" "$temp\_3.zip" -overwrite -zipexe $zipexe | should be $false 
    }

    It "should unzip a file to a directory that is not empty" { 
        test-path "$two\_3.zip" -ea SilentlyContinue | should be $true
        test-path "$three" -ea SilentlyContinue | should be $true
        (gci $three).count | Should BeGreaterThan 0
        $boom = 0
        unzip "$two\_3.zip" "$three\" -overwrite -zipexe $zipexe| should be $true
    }

}
