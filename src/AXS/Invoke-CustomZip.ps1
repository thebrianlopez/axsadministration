$here = Split-Path -Parent $MyInvocation.MyCommand.Path
. "$here\Start-CustomProcess.ps1"

function Invoke-CustomZipCompress{
    param(
        [Parameter(Mandatory=$true,Position=1)][io.directoryinfo]$source,
        [Parameter(Mandatory=$true,Position=2)]$destination="\\CASUNNNAS101.veritix.net\ops\IISLogs\$env:computername",
        [Parameter(Mandatory=$false)][switch]$7z=$false,
        [Parameter(Mandatory=$false)][switch]$overwrite=$false,
        [Parameter(Mandatory=$false)]$zipexe="$env:programfiles\7-zip\7z.exe"
    )

    write-verbose "Starting function"
    if ($PSVersionTable.PSVersion.Major -lt 3){
        $7z=$true
    }

    if((Get-Item $source -erroraction SilentlyContinue).Attributes -ne "Directory"){
        $7z = $true
    }

    switch((Get-Item $destination -erroraction SilentlyContinue).Attributes){
        "Directory" { $destinationzip = ([io.fileinfo]"$destination\$($(Get-Item $source).BaseName).zip") }
        default { $destinationzip = ([io.fileinfo]$destination) }
    }
        
    if(-not (test-path $source -erroraction SilentlyContinue)){ write-error "Unable to find $source"; }
    if(($overwrite) -and (test-path $destinationzip)) { rm -rec -for $destinationzip }

    if($7z) {
        $zipexe = resolve-path $zipexe -erroraction Stop
        Start-CustomProcess "$zipexe" " a `"$destinationzip`" `"$source`""
    }
    else{
        
        add-type -assemblyname "system.io.compression.filesystem"
        [system.io.compression.zipfile]::createfromdirectory($source, $destinationzip)
    }

    switch(test-path $destinationzip -erroraction SilentlyContinue) { 
        "true" { return $true} 
        default { return $false }
    }

}

function Invoke-CustomZipUncompress{
    param(
        [Parameter(Mandatory=$true,Position=1)][io.fileinfo]$source,
        [Parameter(Mandatory=$true,Position=2)][io.fileinfo]$destination,
        [Parameter(Mandatory=$false)][switch]$7z=$false,
        [Parameter(Mandatory=$false)][switch]$overwrite=$false,
        [Parameter(Mandatory=$false)]$zipexe="$env:programfiles\7-zip\7z.exe"
    )

    write-verbose "Starting function"

    if ($PSVersionTable.PSVersion.Major -lt 3){
        $7z=$true
    }

    $ext = (Get-Item $source -erroraction SilentlyContinue).Extension

    switch($ext){
        ".zip" { write-verbose "zip" }
        ".cap" { write-verbose "zip" }
        ".7z"  { write-verbose "zip" }
        default { return $false;write-output "$source isnt a zip file;"}
    }

    write-verbose "Destination is of type $($destination.GetType())"

    # if(-not (test-path $source -erroraction SilentlyContinue)){ write-error "Unable to find $source"; }
    if(($overwrite) -and (test-path $destination)) { rm -rec -for $destination}

    if($7z) {
        $zipexe = resolve-path $zipexe -erroraction Stop
        Start-CustomProcess "$zipExe" " x `"$source`" -o`"$destination`" -y"
    }
    else{
        add-type -assemblyname "system.io.compression.filesystem"
        [system.io.compression.zipfile]::extracttodirectory($source, $destination)
    }

    write-verbose "Ending function"

    switch(test-path $destination -erroraction SilentlyContinue) { 
        "true" { if(@(dir $destination).count -gt 0){return $true}else{return $false}} 
        default { return $false }
    }
}

set-alias -name zip -value Invoke-CustomZipCompress
set-alias -name unzip -value Invoke-CustomZipUncompress
set-alias -name Invoke-CustomZip -value Invoke-CustomZipCompress
set-alias -name Invoke-CustomUnZip -value Invoke-CustomZipUncompress
