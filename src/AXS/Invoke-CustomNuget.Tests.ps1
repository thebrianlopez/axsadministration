$here = Split-Path -Parent $MyInvocation.MyCommand.Path
[io.fileinfo]$fileToTest = "$here\$($MyInvocation.MyCommand.Name.Replace(`"Tests.`",`"`"))"

#set-psdebug -trace 1
#set-psbreakpoint -variable boom

if (-not (test-path $fileToTest)){ write-error "Unable to find the file $fileToTest" }

. $fileToTest
$commandletName = $fileToTest.BaseName



Describe -Tags "Example" "$commandletName" {

    It "should be able to find nuget.exe" { 
        (get-command nuget -erroraction SilentlyContinue) | %{if(test-path $_.Definition){$nuget=$_.Definition}}
        (get-alias nuget -erroraction SilentlyContinue) | %{if(test-path $_.Definition){$nuget=$_.Definition}}
        "$(([io.directoryinfo]$here).parent.parent.fullname)\.nuget\nuget.exe" | %{if(test-path $_){$nuget=$_}}
        "$(([io.directoryinfo]$here).parent.parent.fullname)\.nuget\nuget.config" | %{if(test-path $_){$nugetconfig=$_}}
        $global:nuget = $nuget
        $global:nugetconfig = $nugetconfig
        (test-path $nuget -erroraction SilentlyContinue) | should be true
        (test-path $global:nuget -erroraction SilentlyContinue) | should be true
        (test-path $nugetconfig -erroraction SilentlyContinue) | should be true
        (test-path $global:nugetconfig -erroraction SilentlyContinue) | should be true


    }

    # Disabling this test as it is more of an integration test and not so much testing module
    #It "should be able to pull packages from nuget feed" { 
    #    test-path $global:nuget -erroraction SilentlyContinue | should be true
    #    $cmd = "$($global:nuget) list -source myget -AllVersions -configfile $($global:nugetconfig)"
    #    write-host "Running $cmd" -foregroundcolor Yellow
    #    (iex $cmd).count | should not be 0 
    #}

    It "should be visible on the filesystem" { test-path $fileToTest | Should Be True }

    It "should be accessible by Get-Command" { (get-command Install-CustomPackage).count | should not be 0 }

    It "should be accessible by Get-Command" { (get-command Install-CustomModule).count | should not be 0 }

    # It "should be able to find prerelease packages forcing nuget.exe" { (Install-CustomPackage nlog -o packages -nuget $nuget -forcenuget -allversions) | should not be 0 }

    #It "should be able to find prerelease packages using posh 5" { 
    #    (Install-CustomPackage nlog -nuget $nuget -o packages -allversions) | should not be 0 
    #}

    It "should be able to find prerelease posh modules forcing nuget.exe" { (Install-CustomModule nlog -nuget $nuget -o packages -forcenuget -allversions) | should not be 0 }

    #It "should be able to find prerelease posh modules using version 5" { 
    #    (Install-CustomModule nlog -nuget $nuget -o packages -allversions) | should not be 0 
    #}




}

