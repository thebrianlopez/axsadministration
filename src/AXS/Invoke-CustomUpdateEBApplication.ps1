function Invoke-CustomUpdateEBApplication{
    param(
        [Parameter()][string]$AccessKey="%ElasticBeanstalk.AccessKey%",
        [Parameter()][string]$SecretKey="%ElasticBeanstalk.SecretKey%",
        [Parameter()][string]$Region="%ElasticBeanstalk.Region%",
        [Parameter(Mandatory=$true)][string]$ApplicationName="%ElasticBeanstalk.ApplicationName%",
        [Parameter(Mandatory=$true)][string][ValidateLength(4,32)]$EnvironmentName="%ElasticBeanstalk.EnvironmentName%",
        [Parameter(Mandatory=$false)][string]$SolutionStackName="64bit Windows Server 2012 R2 v1.2.0 running IIS 8.5",
        [Parameter(Mandatory=$true)][string]$BucketName="%S3.BucketName%",
        [Parameter(Mandatory=$true)][system.io.fileinfo]$File,
        [Parameter(Mandatory=$true)][string]$Version="%build.number%",
        [Parameter(Mandatory=$false)][string]$CNAMEPrefix="",
        [Parameter()][string]$Description="%build.vcs.number%"
    )

    <#
    This module has the following dependencies
    1. An ElasticBeanStalk App already exists for the application
    2. An app environment exists with the same name as the current git branch -- if not it will be created
    3. The application binaries must already exist

    Depends on the following AWS PowerShell Modules

    - New-AWSCredentials
    - New-EBEnvironment
    - Get-EBEnvironment
    - Update-EBEnvironment
    - New-EBApplicationVersion 

    #>

    write-verbose "Starting Script"

    $count = 60;
    $incrementSeconds = 10;
    $i = 0;

    $fileNameWithoutExtension       = [System.IO.Path]::GetFileNameWithoutExtension($BaseF)
    $fileExtension                  = [System.IO.Path]::GetExtension($File)
    $newFileName                    = $fileNameWithoutExtension + "." + $version + $fileExtension

    $credparams = @{
        "AccessKey"="$accessKey"
        "SecretKey"="$secretKey"
    }

    $Credential = (New-AWSCredentials @credparams)

    $ebenv = @{
        "Credential"=$Credential;
        "ApplicationName"=$ApplicationName;
        "EnvironmentName"=$EnvironmentName;
        "Region"=$Region;
    }

    $ebapp = @{
        "Credential"=$Credential;
        "ApplicationName"=$ApplicationName;
        "Region"=$Region;
    }

    $ebver = @{
        "Credential"=$Credential;
        "ApplicationName"=$ApplicationName;
        "Region"=$Region;
    }

    write-debug "Step 1: Check to make sure app is valid"
    if(@(Get-EBApplication @ebapp).Count -eq 0){write-error "The application $ApplicationName does not exist in the region $Region" -errorAction Stop}

    write-debug "Step 2a: Validate the S3 bucket is accessable and can be downloaded."

    write-debug "Step 2b: Validate the application version doesnt already exist."
    if(@(Get-EBApplicationVersion @ebver -VersionLabel $Version).Count -eq 0){ 

        write-verbose "Application version $Version does not exist. Creating ..."
        New-EBApplicationVersion @ebver -VersionLabel $Version -SourceBundle_S3Bucket $BucketName -SourceBundle_S3Key $File.Name
        
        do {
            write-verbose "Creating app version $Version. Waiting 30 seconds."
            start-sleep -seconds $incrementSeconds
        }
        while ((Get-EBApplicationVersion @ebver -VersionLabel $Version).Count -eq 0)
    }
    else {
        write-verbose "The app version $Version already exists."
    }

    write-debug "Step 2d: Gather CNAMEPrefix info"
    if(-not ($CNAMEPrefix)){
        $CNAMEPrefix = "$ApplicationName-$EnvironmentName"
    }

    write-debug "Step 3: Create / Update the environment"
    if (@(Get-EBEnvironment @ebenv).count -eq 0) {
        write-verbose "The environment $EnvironmentName does not exist, creating ..."
        New-EBEnvironment @ebenv -CNAMEPrefix $CNAMEPrefix -SolutionStackName $SolutionStackName
        do {
            write-verbose "Spinning up new environment. Waiting 30 seconds."
            start-sleep -seconds $incrementSeconds
        }
        while ((Get-EBEnvironment @ebenv).Status -ne "Ready")
    }
    else{
        write-verbose "Confirmed the environment $EnvironmentName already exists."
    }

    write-debug "Step 8a: Validate application version exists on new environment"
    if(@(Get-EBApplicationVersion @ebver -VersionLabel $Version).Count -eq 0){write-error "The newly updated app version does not exists. Please make sure the app was uploaded to S3" -errorAction Stop}

    write-debug "Step 8b: Validate the application environment is in a healthy state before deploying"
    do {
        write-verbose "Checking if application is in the Green State";
        if($i -eq $count){
            write-error "ElasticBeanstalk converge has timed out at $timeout" -errorAction Stop
            break;
        }
        write-verbose "Waiting $incrementSeconds seconds."
        start-sleep -seconds $incrementSeconds;
    } while((Get-EBEnvironment @ebenv).Health -ne "Green")

    write-debug "Step 8c: Update the environment"
    Update-EBEnvironment @ebenv -VersionLabel $Version -errorAction Stop

    write-debug "Step 9: Validate the version is on the environment and available."
    do {
        write-verbose "Checking if application is in the Green State";
        if($i -eq $count){
            write-error "ElasticBeanstalk converge has timed out at $timeout" -errorAction Stop
            break;
        }
        write-verbose "Waiting $incrementSeconds seconds."
        start-sleep -seconds $incrementSeconds;
    } while((Get-EBEnvironment @ebenv).Health -ne "Green")

    write-verbose "Step 10: Confirm the correct ersion of the application was deployed."
    if(@(Get-EBEnvironment @ebenv -VersionLabel $Version).Count -eq 1) {write-verbose "Environment update succeeded." -errorAction Stop}


    write-verbose "Script completed with error count $($error.count)"
    return $results

}

set-alias -name UpdateEBApp -value Invoke-CustomUpdateEBApplication
<#
$params = @{
    "AccessKey"="xxx"
    "SecretKey"="xxx"
    "Region"="us-east-1"
    "BucketName"="axs.artifacts"
    "Version"="0.0.9"
    "ApplicationName"="axsadministration"
    "EnvironmentName"="release-stage"
    "File"="ReportingScheduler.API.4.18.0+20.zip"
    "CNAMEPrefix"="axsadministration-stage"
}

Measure-Command {
    Invoke-CustomUpdateEBApplication @params -verbose
}
#>

<# here are the available stacks to use 

(Get-EBAvailableSolutionStack -Region us-west-1 -AccessKey XXX -SecretKey xxx ).SolutionStacks

64bit Amazon Linux 2016.09 v2.2.0 running Multi-container Docker 1.11.2 (Generic)
64bit Amazon Linux 2016.09 v3.1.0 running Node.js
64bit Amazon Linux 2015.03 v1.4.6 running Node.js
64bit Amazon Linux 2014.03 v1.1.0 running Node.js
32bit Amazon Linux 2014.03 v1.1.0 running Node.js
64bit Amazon Linux 2016.09 v2.2.0 running PHP 5.4
64bit Amazon Linux 2016.09 v2.2.0 running PHP 5.5
64bit Amazon Linux 2016.09 v2.2.0 running PHP 5.6
64bit Amazon Linux 2016.09 v2.2.0 running PHP 7.0
64bit Amazon Linux 2015.03 v1.4.6 running PHP 5.6
64bit Amazon Linux 2015.03 v1.4.6 running PHP 5.5
64bit Amazon Linux 2015.03 v1.4.6 running PHP 5.4
64bit Amazon Linux 2014.03 v1.1.0 running PHP 5.5
64bit Amazon Linux 2014.03 v1.1.0 running PHP 5.4
32bit Amazon Linux 2014.03 v1.1.0 running PHP 5.5
32bit Amazon Linux 2014.03 v1.1.0 running PHP 5.4
64bit Amazon Linux running PHP 5.3
32bit Amazon Linux running PHP 5.3
64bit Amazon Linux 2016.09 v2.2.0 running Python 3.4
64bit Amazon Linux 2016.09 v2.2.0 running Python
64bit Amazon Linux 2016.09 v2.2.0 running Python 2.7
64bit Amazon Linux 2015.03 v1.4.6 running Python 3.4
64bit Amazon Linux 2015.03 v1.4.6 running Python 2.7
64bit Amazon Linux 2015.03 v1.4.6 running Python
64bit Amazon Linux 2014.03 v1.1.0 running Python 2.7
64bit Amazon Linux 2014.03 v1.1.0 running Python
32bit Amazon Linux 2014.03 v1.1.0 running Python 2.7
32bit Amazon Linux 2014.03 v1.1.0 running Python
64bit Amazon Linux running Python
32bit Amazon Linux running Python
64bit Amazon Linux 2016.09 v2.2.0 running Ruby 2.3 (Puma)
64bit Amazon Linux 2016.09 v2.2.0 running Ruby 2.2 (Puma)
64bit Amazon Linux 2016.09 v2.2.0 running Ruby 2.1 (Puma)
64bit Amazon Linux 2016.09 v2.2.0 running Ruby 2.0 (Puma)
64bit Amazon Linux 2016.09 v2.2.0 running Ruby 2.3 (Passenger Standalone)
64bit Amazon Linux 2016.09 v2.2.0 running Ruby 2.2 (Passenger Standalone)
64bit Amazon Linux 2016.09 v2.2.0 running Ruby 2.1 (Passenger Standalone)
64bit Amazon Linux 2016.09 v2.2.0 running Ruby 2.0 (Passenger Standalone)
64bit Amazon Linux 2016.09 v2.2.0 running Ruby 1.9.3
64bit Amazon Linux 2015.03 v1.4.6 running Ruby 2.2 (Puma)
64bit Amazon Linux 2015.03 v1.4.6 running Ruby 2.2 (Passenger Standalone)
64bit Amazon Linux 2015.03 v1.4.6 running Ruby 2.1 (Puma)
64bit Amazon Linux 2015.03 v1.4.6 running Ruby 2.1 (Passenger Standalone)
64bit Amazon Linux 2015.03 v1.4.6 running Ruby 2.0 (Puma)
64bit Amazon Linux 2015.03 v1.4.6 running Ruby 2.0 (Passenger Standalone)
64bit Amazon Linux 2015.03 v1.4.6 running Ruby 1.9.3
64bit Amazon Linux 2014.03 v1.1.0 running Ruby 2.1 (Puma)
64bit Amazon Linux 2014.03 v1.1.0 running Ruby 2.1 (Passenger Standalone)
64bit Amazon Linux 2014.03 v1.1.0 running Ruby 2.0 (Puma)
64bit Amazon Linux 2014.03 v1.1.0 running Ruby 2.0 (Passenger Standalone)
64bit Amazon Linux 2014.03 v1.1.0 running Ruby 1.9.3
32bit Amazon Linux 2014.03 v1.1.0 running Ruby 1.9.3
64bit Amazon Linux 2016.09 v2.3.1 running Tomcat 8 Java 8
64bit Amazon Linux 2016.09 v2.3.0 running Tomcat 7 Java 7
64bit Amazon Linux 2016.09 v2.3.0 running Tomcat 7 Java 6
64bit Amazon Linux 2015.03 v1.4.5 running Tomcat 8 Java 8
64bit Amazon Linux 2015.03 v1.4.5 running Tomcat 7 Java 7
64bit Amazon Linux 2015.03 v1.4.5 running Tomcat 7 Java 6
64bit Amazon Linux 2014.03 v1.1.0 running Tomcat 7 Java 7
64bit Amazon Linux 2014.03 v1.1.0 running Tomcat 7 Java 6
32bit Amazon Linux 2014.03 v1.1.0 running Tomcat 7 Java 7
32bit Amazon Linux 2014.03 v1.1.0 running Tomcat 7 Java 6
64bit Amazon Linux running Tomcat 7
64bit Amazon Linux running Tomcat 6
32bit Amazon Linux running Tomcat 7
32bit Amazon Linux running Tomcat 6
64bit Windows Server Core 2012 R2 v1.2.0 running IIS 8.5
64bit Windows Server 2012 R2 v1.2.0 running IIS 8.5
64bit Windows Server 2012 v1.2.0 running IIS 8
64bit Windows Server 2008 R2 v1.2.0 running IIS 7.5
64bit Windows Server Core 2012 R2 running IIS 8.5
64bit Windows Server 2012 R2 running IIS 8.5
64bit Windows Server 2012 running IIS 8
64bit Windows Server 2008 R2 running IIS 7.5
64bit Amazon Linux 2016.09 v2.2.0 running Docker 1.11.2
64bit Amazon Linux 2016.03 v2.1.6 running Docker 1.11.2
64bit Amazon Linux 2016.03 v2.1.0 running Docker 1.9.1
64bit Amazon Linux 2015.09 v2.0.6 running Docker 1.7.1
64bit Amazon Linux 2015.03 v1.4.6 running Docker 1.6.2
64bit Debian jessie v2.2.0 running GlassFish 4.1 Java 8 (Preconfigured - Docker)
64bit Debian jessie v2.2.0 running GlassFish 4.0 Java 7 (Preconfigured - Docker)
64bit Debian jessie v1.4.6 running GlassFish 4.1 Java 8 (Preconfigured - Docker)
64bit Debian jessie v1.4.6 running GlassFish 4.0 Java 7 (Preconfigured - Docker)
64bit Debian jessie v2.2.0 running Go 1.4 (Preconfigured - Docker)
64bit Debian jessie v2.2.0 running Go 1.3 (Preconfigured - Docker)
64bit Debian jessie v1.4.6 running Go 1.4 (Preconfigured - Docker)
64bit Debian jessie v1.4.6 running Go 1.3 (Preconfigured - Docker)
64bit Debian jessie v2.2.0 running Python 3.4 (Preconfigured - Docker)
64bit Debian jessie v1.4.6 running Python 3.4 (Preconfigured - Docker)
64bit Amazon Linux 2016.09 v2.2.0 running Java 8
64bit Amazon Linux 2016.09 v2.2.0 running Java 7
64bit Amazon Linux 2016.09 v2.2.0 running Go 1.5
64bit Amazon Linux 2016.03 v2.1.0 running Go 1.4

#>