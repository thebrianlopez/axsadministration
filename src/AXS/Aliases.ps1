# Alisases for cmdine shortcuts

$uisettings = (Get-Host).UI.RawUI
$uisettings.WindowTitle = $("psh > $($env:username)@$($env:computername): ~")
#$uisettings.CursorSize = "100"

# C:\ drive
$location = "$env:systemroot\System32\inetcpl.cpl";if(test-path -path "$location") {Set-Alias int "$location"} # msbuild
$location = "$env:systemroot\Microsoft.NET\Framework\v4.0.30319\msbuild";if(test-path -path "$location") {Set-Alias msbuild "$location"} # msbuild
$location = "${env:ProgramFiles(x86)}\MSBuild\14.0\Bin\MSBuild.exe";if(test-path -path "$location") {Set-Alias msbuild "$location"} 
$location = "$env:systemdrive\Program Files (x86)\Microsoft Visual Studio*\Common7\IDE\devenv.exe";if(test-path -path "$location") {Set-Alias vs "$location"} # Vistual Studio x86
$location = "$env:systemdrive\Program Files (x86)\Git\bin\git.exe";if(test-path -path "$location") {Set-Alias git "$location"} # git x86
$location = "$env:systemdrive\Program Files (x86)\Mozilla Firefox\firefox.exe";if(test-path -path "$location") {Set-Alias fire "$location"} # firefox x86

# Program files
$location = "$env:systemdrive\Program Files\Microsoft SDKs\F#\3.0\Framework\v4.0\fsc.exe";if(test-path -path "$location") {Set-Alias fsc "$location"} # fsharp - fsc
$location = "$env:systemdrive\Program Files\Microsoft SDKs\F#\3.0\Framework\v4.0\fsi.exe";if(test-path -path "$location") {Set-Alias fsi "$location"} # fsharp - fsi
$location = "$env:systemdrive\Program Files\Microsoft SDKs\F#\3.0\Framework\v4.0\fsianycpu.exe";if(test-path -path "$location") {Set-Alias FsiAnyCPU "$location"} # fsharp - FsiAnyCPU
$location = "$env:systemdrive\Program Files\Sublime Text 3\sublime_text.exe";if(test-path -path "$location") {Set-Alias sub "$location"} # sublimetext
$location = "$env:systemdrive\Program Files\Microsoft Visual Studio*\Common7\IDE\devenv.exe";if(test-path -path "$location") {Set-Alias vs "$location"} # VS x64
$location = "$env:systemdrive\Program Files\Git\bin\git.exe";if(test-path -path "$location") {Set-Alias g "$location"};if(test-path -path "$location") {Set-Alias git "$location"} # git x64
$location = "$env:systemdrive\Program Files\Internet Explorer\iexplore.exe";if(test-path -path "$location") {Set-Alias ie "$location"} # ie x64
$location = "$env:systemdrive\Program Files\Mozilla Firefox\firefox.exe";if(test-path -path "$location") {Set-Alias fire "$location"} # firefox x64
$location = "$env:systemdrive\Program Files\IIS\Microsoft Web Deploy*\msdeploy.exe";if(test-path -path "$location") {Set-Alias msdeploy "$location"} # msdeploy
$location = "$env:systemdrive\Program Files\Microsoft Visual Studio*\Common7\IDE\tf.exe";if(test-path -path "$location") {Set-Alias tf "$location"} # teamexplorer
$location = "$env:systemdrive\Program Files\Microsoft Office\Office15\lync.exe";if(test-path -path "$location") {Set-Alias lync "$location"} # lync

# %localappdata%\Programs\XXX\XXX.exe
$location = "$env:localappdata\Programs\SublimeText*\sublime_text.exe";if(test-path -path "$location") {Set-Alias sub "$location"} # Sublime Text 3
$location = "$env:localappdata\Programs\notepad++\notepad++.exe";if(test-path -path "$location") {Set-Alias note "$location"} # Notepad++
$location = "$env:localappdata\Programs\chrome-win32\chrome.exe";if(test-path -path "$location") {Set-Alias ch "$location"} # Chromium-Browser
$location = "$env:localappdata\Programs\WinMergePortable\WinMergePortable.exe";if(test-path -path "$location") {Set-Alias wm "$location"} # WinMergePortable
$location = "$env:localappdata\Programs\git\cmd\git.exe";if(test-path -path "$location") {Set-Alias git "$location"} # Git
$location = "$env:localappdata\Programs\nuget\nuget.exe";if(test-path -path "$location") {Set-Alias nuget "$location"} # nuget 
$location = "$env:localappdata\Programs\pandoc\pandoc.exe";if(test-path -path "$location") {Set-Alias pandoc "$location"} # Pandoc

# %userprofile%\Downloads\XXX\XXX.exe
$location = "$env:userprofile\Downloads\SublimeText3\sublime_text.exe";if(test-path -path "$location") {Set-Alias sub "$location"} # Sublime Text 3
$location = "$env:userprofile\Downloads\notepad++\notepad++.exe";if(test-path -path "$location") {Set-Alias note "$location"} # Notepad++
$location = "$env:userprofile\Downloads\chrome-win32\chrome.exe";if(test-path -path "$location") {Set-Alias ch "$location"} # Chromium-Browser
$location = "$env:userprofile\Downloads\WinMergePortable\WinMergePortable.exe";if(test-path -path "$location") {Set-Alias wm "$location"} # WinMergePortable
$location = "$env:userprofile\Downloads\git\cmd\git.exe";if(test-path -path "$location") {Set-Alias git "$location"} # Git
$location = "$env:userprofile\Downloads\nuget\nuget.exe";if(test-path -path "$location") {Set-Alias nuget "$location"} # nuget 
$location = "$env:userprofile\Downloads\pandoc\pandoc.exe";if(test-path -path "$location") {Set-Alias pandoc "$location"} # Pandoc
#[io.fileinfo]((resolve-path "$env:userprofile\Downloads\*\dig.exe")[0]).Path | %{if(test-path -path $_ ){Set-Alias -name $_.BaseName -value $_.FullName}}
#[io.fileinfo]((resolve-path "$env:userprofile\Downloads\*\consul.exe")[0]).Path | %{if(test-path -path $_ ){Set-Alias -name $_.BaseName -value $_.FullName}}
#[io.fileinfo]((resolve-path "$env:userprofile\Downloads\jq*.exe")[0]).Path | %{if(test-path -path $_ ){Set-Alias -name $_.BaseName -value $_.FullName}}
# Nuget Sources - Userprofile

# Roamig Appdata
$location = "$env:appdata\Nuget\Programs\SublimeText*\content\sublime_text.exe";if(test-path -path "$location") {Set-Alias sub "$location"} # Sublime Text 3
$location = "$env:appdata\Nuget\Programs\notepad*\content\notepad++.exe";if(test-path -path "$location") {Set-Alias note "$location"} # Notepad++
$location = "$env:appdata\Nuget\Programs\npp*\content\notepad++.exe";if(test-path -path "$location") {Set-Alias npp "$location"} # Notepad++
$location = "$env:appdata\Nuget\Programs\chrome-win32*\content\chrome.exe";if(test-path -path "$location") {Set-Alias ch "$location"} # Chromium-Browser
$location = "$env:appdata\Nuget\Programs\chromium-browser*\*\chrome.exe";if(test-path -path "$location") {Set-Alias ch "$location"} # Chromium-Browser
$location = "$env:appdata\Nuget\Programs\WinMergePortable*\content\WinMergePortable.exe";if(test-path -path "$location") {Set-Alias wm "$location"} # WinMergePortable
$location = "$env:appdata\Nuget\Programs\git*\content\cmd\git.exe";if(test-path -path "$location") {Set-Alias git "$location"} # Git
$location = "$env:appdata\Nuget\Programs\git*\content\bin\ssh-keygen.exe";if(test-path -path "$location") {Set-Alias ssh-keygen "$location"} # Git
$location = "$env:appdata\Nuget\Programs\pandoc*\content\pandoc.exe";if(test-path -path "$location") {Set-Alias pandoc "$location"} # Pandoc
$location = "$env:appdata\Nuget\Programs\RDCMan*\content\RDCMan.exe";if(test-path -path "$location") {Set-Alias rdcman "$location"} # RDCMan
$location = "$env:appdata\Nuget\Programs\RDCMan*\content\RDCMan.exe";if(test-path -path "$location") {Set-Alias rdm "$location"} # RDCMan
$location = "$env:appdata\Nuget\Programs\putty*\content\putty.exe";if(test-path -path "$location") {Set-Alias putty "$location"} # Putty
$location = "$env:appdata\Nuget\Programs\firefox*\content\firefox.exe";if(test-path -path "$location") {Set-Alias firefox "$location"} # Firefox
$location = "$env:appdata\Nuget\Programs\firefox*\content\firefox.exe";if(test-path -path "$location") {Set-Alias fire "$location"} # Firefox
$location = "$env:appdata\Nuget\Programs\nuget*\content\nuget.exe";if(test-path -path "$location") {Set-Alias nuget "$location"} # NuGet
$location = "$env:appdata\Nuget\Programs\clearpxe*\content\ClearPxeFlag.exe";if(test-path -path "$location") {Set-Alias clearpxe "$location"} # ClearPxe
$location = "$env:appdata\Nuget\Programs\clearpxe*\ClearPxeFlag.exe";if(test-path -path "$location") {Set-Alias clearpxe "$location"} # ClearPxe
$location = "$env:appdata\Nuget\Programs\nugetpackageexplorer*\content\NuGetPackageExplorer.exe";if(test-path -path "$location") {Set-Alias npe "$location"} # NuGetPackageExplorer
$location = "$env:appdata\Nuget\Programs\nugetpackageexplorer*\content\NuGetPackageExplorer.exe";if(test-path -path "$location") {Set-Alias nugetpackageexplorer "$location"} # NuGetPackageExplorer
$location = "$env:appdata\NuGet\Programs\SCCMClientCenter\content\SMSCliCtrV2.exe";if(test-path -path "$location") {Set-Alias sccmclientcenter "$location"} # SCCM Client Center
$location = "$env:appdata\NuGet\Programs\CCMTools*\ClientTools\CMTrace.exe";if(test-path -path "$location") {Set-Alias cmtrace "$location"} # CMTrace
$location = "$env:appdata\NuGet\Programs\7z*\7zFM.exe";if(test-path -path "$location") {Set-Alias 7zfm "$location"} # CMTrace
$location = "$env:appdata\NuGet\Programs\procmon*\content\procmon.exe";if(test-path -path "$location") {Set-Alias procmon "$location"} # CMTrace
$location = "$env:appdata\NuGet\Programs\octo*\octo.exe";if(test-path -path "$location") {Set-Alias octo "$location"} # CMTrace
$location = "$env:appdata\NuGet\Programs\filezilla*\content\filezilla.exe";if(test-path -path "$location") {Set-Alias fz "$location"} # filezilla
$location = "$env:appdata\NuGet\Programs\filezilla*\content\filezilla.exe";if(test-path -path "$location") {Set-Alias filezilla "$location"} # filezilla


function invoke-killall{
  param(
    [Parameter(Position=0,mandatory=$true)][string]$process
    )

  get-process $process -errorAction SilentlyContinue | stop-process 

}

set-alias psh Enter-PSSession
Set-Alias -name cleanupazuredisks -value Remove-cAzureDisks
Set-Alias -name wget -value Start-filedownload -option AllScope
Set-Alias -name Unzip-File -value Expand-File
Set-Alias -name glb -value repstatus
set-alias -name disableproxy -value Set-CustomProxy
set-alias -name proxyctrl -value Set-CustomProxy
set-alias -name killall -value Invoke-killall
set-alias -name repstatus -value Invoke-repstatus
set-alias -name crs -value Invoke-crs
set-alias -name ts -value Invoke-ts
set-alias -name inst -value Install-CustomPackage
set-alias -name modinst -value Install-CustomModule
set-alias -name pinst -value Install-CustomModule
set-alias -name minst -value Install-CustomModule
set-alias -name psinst -value Install-CustomModule
set-alias -name psminst -value Install-CustomModule
