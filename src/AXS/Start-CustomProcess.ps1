function Start-CustomProcess {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)][System.IO.FileInfo]$cmd,
        [Parameter(Mandatory=$true)][string]$arg
        )
    if(-not (test-path $cmd)){write-error "Unable to find $cmd" -ea Stop}
    $StartInfo = new-object System.Diagnostics.ProcessStartInfo
    $StartInfo.FileName = "$cmd"
    $StartInfo.Arguments = "$arg"
    $StartInfo.UseShellExecute = $false
    $StartInfo.WorkingDirectory = "$env:systemdrive"
    write-verbose "Running: $cmd $arg"
    
    $StartInfo | foreach-object {
        if($PSCmdlet.ShouldProcess($_,"Run command? $($_.FileName) $($_.Arguments)")){
            [int]$exitCode = [Diagnostics.Process]::Start($StartInfo).WaitForExit()
            if($exitCode -eq 0){
                write-verbose "command complete successfully."
                $results = $true
            }
            else{
                write-error "command existed with code $($_.Exception.Message.toString())" -ErrorAction Stop
                throw
            }
        }
    }
    $cmd = $null 
    $arg = $null 
}