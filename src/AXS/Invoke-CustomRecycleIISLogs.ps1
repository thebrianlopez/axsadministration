function Invoke-CustomRecycleIISLogs {
    param(
        [Parameter(Mandatory=$false)]$source="IIS:\Sites",
        [Parameter(Mandatory=$true,Position=0)]$destination,
        [Parameter(Mandatory=$false)][switch]$overwrite=$false
        )

    $result = @()

    (dir $source ) | %{ 

        $src = $((Get-ItemProperty $_.PSPath -name logfile).directory)

        $src = (iex "cmd /c 'echo $src'")

        $name =  $($(Get-ItemProperty $_.PSPath -name logfile).PSChildName)

        $dst = "$destination\$($(Get-ItemProperty $_.PSPath -name logfile).PSChildName).zip"

        $zipParams = @{
            "source"="$src";
            "destination"="$dst";
        }

        if($overwrite){$zipParams.Add("overwrite",$true)}
        
        $props = @{
            "Source"="$src";
            "Destination"="$dst";
            "Result"=$(Invoke-CustomZipCompress @zipParams);
        }

        $result += New-Object -TypeName PSOBject -Property $props
        $destination
    }

    return $result
}

function Invoke-CustomFileCleanUp {
    [CmdletBinding(SupportsShouldProcess=$true)]
    param(
        [Parameter(Mandatory=$false)]$source="IIS:\Sites",
        [Parameter(Mandatory=$false)][string]$extension="*.log".Replace("*",""),
        [Parameter(Mandatory=$false)][system.datetime]$timespan=(Get-Date).AddDays(-30),
        [Parameter(Mandatory=$false)][switch]$force
        )

    $results = @()
    $i = 1
    write-verbose "Starting script."

    (dir $source ) | %{ 

        $src = $((Get-ItemProperty $_.PSPath -name logfile).directory)
        $src = (iex "cmd /c 'echo $src'")

        write-verbose "Starting interation $i : Checking the path $src for files older than $timespan with $extension extension"


        Get-ChildItem -Path $src -Recurse -Force | Where-Object { 
            ($_.LastWriteTime -lt $timespan) -and ($_.Extension -like $extension)
        } | %{
            
            Get-Item -Path $_.FullName  | Where-Object { !$_.PSIsContainer } | %{

                $props = @{
                    "Index"="$i";
                    "FullName"=$_.FullName;
                    "LastWriteTime" = $_.LastWriteTime;
                    "CreationTime" = $_.CreationTime;
                    "Attributes" = $_.Attributes;
                    "Action" = "Delete";
                }

                $results += New-Object -TypeName PSOBject -Property $props
                write-verbose "Deleting the file $($_.FullName)"
                if ($PSCmdlet.ShouldProcess($_.FullName,"Remove File")) {  
                    (Get-Item $_.FullName | Remove-Item -Force:$force -confirm:$false)
                }
                
            }
        }

        # Delete any empty directories left behind after deleting the old files.
        Get-ChildItem -Path $src -Recurse -Force | Where-Object {
                $_.PSIsContainer -and (Get-ChildItem -Path $_.FullName -Recurse -Force | Where-Object { !$_.PSIsContainer }) -eq $null 
        } | %{

            $props = @{
                    "Index"="$i";
                    "FullName"=$_.FullName;
                    "LastWriteTime" = $_.LastWriteTime;
                    "CreationTime" = $_.CreationTime;
                    "Attributes" = $_.Attributes;
                    "Action" = "Delete";
            }

            $results += New-Object -TypeName PSOBject -Property $props
            write-verbose "Deleting the empty folder $($_.FullName)";
            if ($PSCmdlet.ShouldProcess($_.FullName,"Remove Directory")) {  
                (Get-Item $_.FullName | Remove-Item -Force:$force -confirm:$false -Recurse)
            }
        }
        $i++;
    }

    write-verbose "Ending script."
}

set-alias -name rolliislogs -value Invoke-CustomRecycleIISLogs
set-alias -name backupiislogs -value Invoke-CustomRecycleIISLogs
set-alias -name deleteiislogs -value Invoke-CustomFileCleanUp
