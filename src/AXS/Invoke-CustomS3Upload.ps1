function Invoke-CustomS3Upload{
    param(
        [Parameter(Mandatory=$true)][string]$AccessKey,
        [Parameter(Mandatory=$true)][string]$SecretKey,
        [Parameter(Mandatory=$true)][string]$Region,
        [Parameter(Mandatory=$true)][string]$BucketName,
        [Parameter(Mandatory=$true)][system.io.fileinfo[]]$LocalFiles,
        [Parameter(Mandatory=$false)][string]$Folder="/",
        [Parameter(Mandatory=$false)][string]$Version
        )

        write-verbose "Building AWS Credential Object"

        $Credential = New-AWSCredentials -AccessKey $AccessKey -SecretKey $SecretKey

        $results = @()

        $LocalFiles | %{

            write-host "File is $_"
        

            if (Test-Path $_.FullName) {

                write-verbose "Path $($_.FullName) exists"
            
                if($Version){ 
                    write-verbose "verion is speciied then rename the file to the correct version and upload"
                    $fileNameWithoutExtension = [System.IO.Path]::GetFileNameWithoutExtension($_.FullName)
                    $fileExtension = [System.IO.Path]::GetExtension($_.FullName)
                    $newFileName = $fileNameWithoutExtension + "." + $Version + $fileExtension
                    [io.fileinfo]$newFile = "$env:temp\$newFileName"
                    copy $_ $newFile -force -verbose
                    $localfile = [io.fileinfo]$newFile

                }
                else {
                    $localfile = [io.fileinfo]$_.FullName
                }

                $key = "$folder/$(([io.fileinfo]$localfile).Name)".Replace("\\","\").Replace("//","/")
                
                $s3object = @{
                    "BucketName"=$BucketName
                    "Key"=$key                  #this is the remote folder path eg bucket/folder1/folder2/file.txt
                    "File"=$localfile              
                    "Region"=$Region
                    "Credential"=$Credential
                }

                write-verbose "Copying $localfile to $BucketName$key"

                Write-S3Object @s3object -verbose
            
                #$results = $true
            }
            else {
                write-error "The file $($_.FullName) does not exist."
                #$results = $false
                exit 1
            }

            $results += New-Object -TypeName PSOBject -Property $s3object
        }

        return $results

}

set-alias -name UploadTos3 -value Invoke-CustomS3Upload

<#
$tcparams = @{
    "AccessKey"="%S3.AccessKey%"
    "SecretKey"="%S3.SecretKey%"
    "Region"="%S3.Region%" 
    "BucketName"="%S3.BucketName%"
    "Version"="%build.number%"
}

Measure-Command {
    Invoke-CustomS3Upload @devparams -verbose
}
#>
