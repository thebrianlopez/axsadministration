param(
    [Parameter(Mandatory=$True)]
    [string]$Branch,
    [Parameter(Mandatory=$True)]
    [string]$Directory,
    [Parameter(Mandatory=$True)]
    [string]$RepoClient
)

$PowershellScriptDirectory = 'C:\Users\michael.tarleton\Documents\PowerShell'

# Sourcing external powershell objects and methods.
. $PowershellScriptDirectory\RepositoryPowershellFunctions.ps1

Format-VXRepositoryLabel -Branch $Branch -Directory $Directory -RepoClient $RepoClient