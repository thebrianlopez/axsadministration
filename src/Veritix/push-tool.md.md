# Deploy Script #

## Capabilities ##
- Get, Start, Stop services (**WMI**)
- Check what product versions are going out from build server
- Check SQL content going out
- Check which servers to send content to
- Push
	- select environment
	- select product

## Specifications ##
- If ComputerName and Environment are used together then combine them.

## PROGRAM ##
- *[string]***$Action** *(NULL)*
	- *[Verb]*-*[Noun]* 
	- *Get-ProductVersions*
	- *Get-ServerList*
	- *Get-Services*
	- *Stop-Services*
	- *Start-Services*
	- *Push-Files*
	- *Backup-Files*
- *[string]***$Product[]** *(NULL)*
	- if no product is specified then show options and prompt user (this is a mandatory field)
	- help file shows which products are available
	- *AccessServer*
	- *AdTrackingContentService*
	- *API*
	- *AutoBlocker*
	- *AutoUpdate*
	- *BackOfficeClient*
	- *BackOfficeWeb*
	- *BoxOfficeClient*
	- *ClientPortal*
	- *CustomerPortal*
	- *ExternalCCProcessors*
	- *FlashSeats*
	- *IMS*
	- *Integration*
	- *KioskClient*
	- *LogHarvest*
	- *LogRelay*
	- *LunarMonkey*
	- *Monetra*
	- *OLAP*
	- *OTS*
	- *PrintTool*
	- *ServiceHost*
	- *SpaceBison*
	- *Statdis*
	- *SuiteLockApplication*
	- *UHH*
	- *VAScheduler*
	- *VenueServer*
- *[string]***ComputerName[]** *(NULL)*
- *[string]***Environment[]** *(NULL)*
	- gets the appropriate server list
	- I.e. **qa-server-list.txt**
- Log results
	- log what files were copied, services were stopped, services started
- Email results 

## Services ##

### Get-Services, Start-Services, Stop-Services ###
- Check for Environment else ComputerName
	- If Environment Then use [env]-server-list.txt
	- If ComputerName Then use ComputerName array
- Check for Services
	- If Services Then use them
	- Else use services.txt

### Restart IIS ###
- Restart all IIS servers.
- http://dalqaapp01/admin/services.aspx