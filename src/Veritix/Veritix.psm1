<#
.EXAMPLE
Stop-VXServices -Environment QA

.EXAMPLE
Start-VXServices -Environment QA 

.EXAMPLE
Get-VXService -Environment QA

.EXAMPLE
Deploy-VXFiles -Environment Stage -Application FlashSeats

Todo
----
* Start flashseats admin/services.aspx
* update config files

#>

##### UTILITIES ####
function Get-VXDate
{
	[CmdletBinding()]
	param 
	(
		[string] $Format,
		[switch] $Date,
		[switch] $Time
	)
	
	[string] $DefaultDateFormat = "yyyy-MM-dd"
	[string] $DefaultTimeFormat = "hh-mm-ss"
	
	if ( [string]::IsNullOrEmpty($Format) )
	{
		if ( $Date )
		{
			$Format = $DefaultDateFormat
		}
		else
		{
			$Format = $DefaultTimeFormat
		}
	}
	
	Get-Date -Format $Format
}

function Get-VXCredential 
{
    [CmdletBinding()]
    param(
        [string] $Environment,
        [System.Management.Automation.PSCredential] $Credential
    );
    
    $credentialVariableName = "$Environment`Credentials";
    
    if ( -not $Credential )
    {
        Write-Verbose "Checking for the global credential: $Environment`Credentials";
        
        $Credential = Get-Variable -Name "$Environment`Credentials" -Scope Global -ErrorAction SilentlyContinue -ValueOnly;
        
        if ( -not $Credential )
        {
            while ( -not $Credential )
            {
                $Credential = Get-Credential $([System.Security.Principal.WindowsIdentity]::GetCurrent().Name);
            }
        
            Set-Variable -Scope Global -Name $credentialVariableName -Value $Credential;
        }
    }
    
    return $credential;
}

function Get-VxServiceNames
{
    [CmdletBinding()]
    param(
        [string[]] $ServicePattern,
        [switch] $IncludeDefaultServicePatterns
    )
    
    [string[]] $defaultIncludeServicePatterns = @("*va*scheduler*","*ims*","*vxsh*","*veritix*");
    [string[]] $completeIncludeServicePattern = @();
    [string] $tmp = "";
    
    if ( $ServicePattern )
    {
        foreach ( $pattern in $ServicePattern )
        {
            $completeIncludeServicePattern += $pattern;
        }
    }
    
    if ( $IncludeDefaultServicePatterns -or -not $ServicePattern )
    {
        foreach ( $pattern in $defaultIncludeServicePatterns )
        {
            $completeIncludeServicePattern += $pattern;
        }
    }
        
    Write-Verbose "Service pattern count: $($completeIncludeServicePattern.Count)";
    
    return $completeIncludeServicePattern;
}

function Get-VxComputerName
{
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        [string[]] $Environment,
        [string[]] $ComputerName
    )
    
    [string]$defaultEnvironment = "qa";
	[string]$WindowsPowershellHomeDirectory = "\Windows\System32\WindowsPowerShell\v1.0";
	[string]$ModulesDirectory = "Modules";
	[string]$VeritixModuleDirectory = "Veritix";
    [string]$pathPrefix = "$WindowsPowershellHomeDirectory\$ModulesDirectory\$VeritixModuleDirectory";
    [string]$pathSuffix = "-server-list.txt";
    [string[]]$completeComputerNameList = @();
	
	Write-Verbose "Server list directory: $pathPrefix"
    
    if ( $Environment )
    {
        foreach ( $env in $Environment )
        {
            $fullName = "$pathPrefix\$env$pathSuffix";
            
            if ( Test-Path -Path $fullName )
            {
                Write-Verbose "Environment: $env";
				Write-Verbose "Environment server list file name: $fullName";
				
                $completeComputerNameList += Get-Content -Path $fullName;
				
                Write-Verbose "Computer Count: $($completeComputerNameList.Count)";
            }
        }
    }
    
    if ( $ComputerName )
    {
        foreach ( $computer in $ComputerName )
        {
            Write-Verbose "Computer Name: $computer"
            $completeComputerNameList += $computer
            Write-Verbose "Computer Count: $($completeComputerNameList.Count)"
        }
    }
    
    return $completeComputerNameList
}

function Get-VXServiceFormatFilter
{
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        [string[]] $Service
    )
    
    [string] $filter = '';

    foreach ( $svc in $Service )
    {
        # Replace the '*' with '%' in the filter
        if ( $filter.Length -eq 0 )
        {
            $filter += "Name LIKE '$($svc.Replace("*","%"))'";
        }
        else
        {
            $filter += " OR Name LIKE '$($svc.Replace("*","%"))'";
        }
    }
    
    return $filter;
}

function Sort-VXServices
{
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        [System.Management.ManagementObject[]] $Service,
        [string[]] $SelectProperties,
        [string[]] $SortProperties,
        [string[]] $FormatProperties,
        [switch]   $Default
    )
    
    if ( $Default -or -not ( $SelectProperties -and $SortProperties -and $FormatProperties ) )
    {
        $SelectProperties = @('State','SystemName','Name','PathName');
        $SortProperties = $SelectProperties;
        $FormatProperties = $SelectProperties;
    }   
    
    return ( $Service `
                | Select $SelectProperties `
                | Sort $SortProperties `
                | Format-Table $FormatProperties -AutoSize );
}

function Filter-ServiceState
{
    [CmdletBinding()]
    param(
        [System.Management.ManagementBaseObject[]] $Service
    )
    
    $filteredResults = $Service |
                        Select ReturnValue |`
                        Sort ReturnValue |`
                        Format-Table -AutoSize;
    
    return $filteredResults;
}


#### END OF UTILITIES ####

<#
    .SYNOPSIS 
    Worker function for stopping all Veritix serives on multiple servers.
    
    .PARAMETER Environment
    Which enrionments you want to act on (i.e. QA or STAGE)
    
    .PARAMETER ComputerName
    Which computers you want to act on. If you specified an environment this will 
    append to those computers.
    
    .PARAMETER Credential
    This is the credential for acting on servers on a different domain.
    (i.e. use stage\<username>)
    
    .EXAMPLE 
    Get-WorkerVXServices -Environment QA 
    
    .EXAMPLE 
    Get-WorkerVXServices -Environment STAGE -Credential stage\mtarleton
    
    .EXAMPLE 
    Get-WorkerVXServices -ComputerName dalstagebo01.stage.local,dalstagebo02.stage.local
    
    .EXAMPLE 
    Get-WorkerVXServices -Environment STAGE -ComputerName dalstagebo01.stage.local,dalstagebo02.stage.local
#>
function Get-VXService
{ 
    [CmdletBinding()]
    param(
        [Parameter(HelpMessage="Enter the environments you want to deploy to")]
        [string[]] $Environment,
        [string[]] $ComputerName, 
        [string[]] $Service,
        [string[]] $SelectProperties,
        [string[]] $SortProperties,
        [string[]] $FormatProperties,
        [switch]   $IncludeDefaultServicePatterns,
        [System.Management.Automation.PSCredential] $Credential,
        [switch]   $DefaultSelectSortAndFormat,
        [switch]   $DoNotFilter,
        [switch]   $StopService,
        [switch]   $StartService,
		[switch]   $IncludeIIS
    )
    
    [string[]] $servicesToSearchFor = Get-VxServiceNames -ServicePattern $Service -IncludeDefaultServicePatterns:$IncludeDefaultServicePatterns;
    [string] $filter = Get-VXServiceFormatFilter -Service $servicesToSearchFor;
    [string[]] $defaultSelectAndSortProperty = @('State','SystemName','Name');
    [System.Management.ManagementObject[]] $allReturnedServices;
    
    Write-Verbose "Service Filter: $filter";
    
    if ( $Environment )
    {
        # Get the server list.
        $ComputerName += Get-VxComputerName -Environment $Environment -ComputerName $ComputerName;
    }
	
    if ( $Environment -contains "STAGE" )
    {
        $Credential = Get-VXCredential -Environment "STAGE" -Credential $Credential;
        
        Write-Verbose "Ready to run: Get-WmiObject -Class win32_service -Filter $filter -ComputerName $ComputerName -Credential $($Credential.Username)"
        
        $allReturnedServices += Get-WmiObject -Class win32_service -Filter "$filter" -ComputerName $ComputerName -Credential $Credential
    }
    else
    {        
        # Get the services
        Write-Verbose "Ready to run: Get-WmiObject -Class win32_service -Filter $filter -ComputerName $ComputerName"
        
        $allReturnedServices += Get-WmiObject -Class win32_service -Filter "$filter" -ComputerName $ComputerName 
    }
    
    if ( -not $DoNotFilter )
    {
        if ( $DefaultSelectSortAndFormat )
        {
            $allReturnedServicesSorted = Sort-VXServices -Service $allReturnedServices -Default;
        }
        else
        {
            $allReturnedServicesSorted = Sort-VXServices `
                                    -Service $allReturnedServices `
                                    -SelectProperties $SelectProperties `
                                    -SortProperties $SortProperties `
                                    -FormatProperties $FormatProperties;
        }
    
        return $allReturnedServicesSorted;
    }
    else
    {
        return $allReturnedServices;
    }
}

<#
function Make-StartServiceReturnValueReadable 
{
    [CmdletBinding()]
    param(
        [enum] $ReturnValue
    )
    
    
    0
Success
1
Not Supported
2
Access Denied
3
Dependent Services Running
4
Invalid Service Control
5
Service Cannot Accept Control
6
Service Not Active
7
Service Request Timeout
8
Unknown Failure
9
Path Not Found
10
Service Already Running
11
Service Database Locked
12
Service Dependency Deleted
13
Service Dependency Failure
14
Service Disabled
15
Service Logon Failure
16
Service Marked For Deletion
17
Service No Thread
18
Status Circular Dependency
19
Status Duplicate Name
20
Status Invalid Name
21
Status Invalid Parameter
22
Status Invalid Service Account
23
Status Service Exists
24
Service Already Paused
}

#>

function Start-VXService
{ 
    [CmdletBinding()]
    param(
        [Parameter(HelpMessage="Enter the environments you want to deploy to")]
        [string[]] $Environment,
        [string[]] $ComputerName, 
        [string[]] $Service,
        [string[]] $SelectProperties,
        [string[]] $SortProperties,
        [string[]] $FormatProperties,
        [switch]   $IncludeDefaultServicePatterns,
        [System.Management.Automation.PSCredential] $Credential,
        [switch]   $DefaultSelectSortAndFormat,
		[switch]   $IncludeIIS
    )
    
    Write-Verbose "Running Start-VXService";
	
	if ( $IncludeIIS )
	{
		Invoke-Command `
			-ComputerName $(Get-VXComputerName -Environment $Environment -ComputerName $ComputerName)`
			-ScriptBlock { iisreset /start };
	}
    
    [System.Management.ManagementObject[]] $services = Get-VXService `
        -Environment $Environment `
        -ComputerName $ComputerName `
        -Service $Service `
        -Credential $Credential `
        -DoNotFilter; 
    
    $invocationResults = $services | Invoke-WmiMethod -Name StartService -ErrorAction SilentlyContinue;
    
    $filteredResults = Filter-ServiceState -Service $invocationResults;
        
    Write-Verbose "Done!";
    
    return $filteredResults
}

function Stop-VXIIS
{
    [CmdletBinding()]
    param(
        [string[]] $Environment,
        [string[]] $ComputerName,
        [System.Management.Automation.PSCredential] $Credential
    )
    
    Write-Verbose "Running Stop-VXIIS";
	
	Invoke-Command `
		-ComputerName $(Get-VXComputerName -Environment $Environment -ComputerName $ComputerName)`
		-ScriptBlock { iisreset /stop };
		
	Write-Verbose "Done!"
}

function Start-VXIIS
{
    [CmdletBinding()]
    param(
        [string[]] $Environment,
        [string[]] $ComputerName,
        [System.Management.Automation.PSCredential] $Credential
    )
    
    Write-Verbose "Running Start-VXIIS";
	
	Invoke-Command `
		-ComputerName $(Get-VXComputerName -Environment $Environment -ComputerName $ComputerName)`
		-ScriptBlock { iisreset /start };
		
	Write-Verbose "Done!"
}

function Start-VXIIS
{
    [CmdletBinding()]
    param(
        [string[]] $Environment,
        [string[]] $ComputerName,
        [System.Management.Automation.PSCredential] $Credential
    )
    
    Write-Verbose "Running Get-VXIISStatus";
	
	Invoke-Command `
		-ComputerName $(Get-VXComputerName -Environment $Environment -ComputerName $ComputerName)`
		-ScriptBlock {`
			Write-Output $(hostname);`
			iisreset /status`
		};
		
	Write-Verbose "Done!"
}

function Stop-VXService
{ 
    [CmdletBinding()]
    param
	(
        [string[]] $Environment,
        [string[]] $ComputerName, 
        [string[]] $Service,
        [string[]] $SelectProperties,
        [string[]] $SortProperties,
        [string[]] $FormatProperties,
        [switch]   $IncludeDefaultServicePatterns,
        [System.Management.Automation.PSCredential] $Credential,
        [switch]   $DefaultSelectSortAndFormat
    )
    
    Write-Verbose "Running Stop-VXService";
    
    [System.Management.ManagementObject[]] $services = Get-VXService `
        -Environment $Environment `
        -ComputerName $ComputerName `
        -Service $Service `
        -Credential $Credential `
        -DoNotFilter; 
    
    $invocationResults = $services | Invoke-WmiMethod -Name StopService -ErrorAction SilentlyContinue;
    
    $filteredResults = Filter-ServiceState -Service $invocationResults;
        
    Write-Verbose "Done!";
    
    return $filteredResults
}

function Restart-VXService
{ 
    [CmdletBinding()]
    param(
        [Parameter(HelpMessage="Enter the environments you want to deploy to")]
        [string[]] $Environment,
        [string[]] $ComputerName, 
        [string[]] $Service,
        [string[]] $SelectProperties,
        [string[]] $SortProperties,
        [string[]] $FormatProperties,
        [switch]   $IncludeDefaultServicePatterns,
        [System.Management.Automation.PSCredential] $Credential,
        [switch]   $DefaultSelectSortAndFormat,
		[switch]   $ShowServiceStatusAfterComplete
    )
    
    Write-Verbose "Restarting VX Services...";
	
    Stop-VXService `
		-Environment $Environment `
		-ComputerName $ComputerName `
		-Service $Service `
		-SelectProperties $SelectProperties `
		-SortProperties $SortProperties `
		-FormatProperties $FormatProperties `
		-IncludeDefaultServicePatterns:$IncludeDefaultServicePatterns `
		-Credential $Credential `
		-DefaultSelectSortAndFormat:$DefaultSelectSortAndFormat
		
	Start-VXService `
		-Environment $Environment `
		-ComputerName $ComputerName `
		-Service $Service `
		-SelectProperties $SelectProperties `
		-SortProperties $SortProperties `
		-FormatProperties $FormatProperties `
		-IncludeDefaultServicePatterns:$IncludeDefaultServicePatterns `
		-Credential $Credential `
		-DefaultSelectSortAndFormat:$DefaultSelectSortAndFormat
		
	if ( $ShowServiceStatusAfterComplete )
	{
		Get-VXService `
		-Environment $Environment `
		-ComputerName $ComputerName `
		-Service $Service `
		-SelectProperties $SelectProperties `
		-SortProperties $SortProperties `
		-FormatProperties $FormatProperties `
		-IncludeDefaultServicePatterns:$IncludeDefaultServicePatterns `
		-Credential $Credential `
		-DefaultSelectSortAndFormat:$DefaultSelectSortAndFormat
	}
		
    Write-Verbose "Done!";
    
    return $filteredResults
}

function Deploy-VXProduct
{
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        [ValidateSet("QA","STAGE","PROD")]
        [string[]]$Environment,
        [Parameter(Mandatory=$True)]
        [string[]]$Application,
        [System.Management.Automation.PSCredential]$Credential,
        [ValidateSet("Packaged","Compiled")]
        [String]$Package,
        [String]$Source,
        [String[]]$Destination,
        [String[]]$ComputerName, 
        [String[]]$Include = @("*.exe","*.dll","*.aspx","*.ascx")
    )
}

function Get-VXApplicationDeployedLocation
{
	[CmdletBinding()]
	param 
	(
		[string] $Environment,
		[string] $ComputerName,
		[string] $Application,
		[string] $XPath,
		[string] $XMLPath
	)
	
	$Environment = $Environment.ToUpper();
	$DefaultXPath = "//Environment[Name = '$Environment']/*/Application[Name = '$Application']"
	$DefaultXMLPath = "C:\Windows\System32\WindowsPowerShell\v1.0\Modules\Veritix\server-list.xml"
	
	if ( [string]::IsNullOrEmpty($XPath) )
	{
		$XPath = $DefaultXPath
	}
	
	if ( [string]::IsNullOrEmpty($XMLPath) )
	{
		$XMLPath = $DefaultXMLPath
	}
	
	$XmlNode = $(Select-Xml -Path $XMLPath -XPath $XPath).Node
	
	$ApplicationDeploymentPath = $XmlNode.DeployedLocation
	
	return $ApplicationDeploymentPath
}

function Backup-VXApplicationWorker
{
	[CmdletBinding()]
	param
	(
        [string] $Environment,
        [string] $ComputerName, 
        [string] $Application,
		[string] $Destination,
		[string] $Source,
        [System.Management.Automation.PSCredential] $Credential
    )
	
	
	[string]   $BackupExtension = ".zip"
	[string]   $UNCSourcePrefix = "\\" + $ComputerName + "\C$\"
	[string]   $DefaultDestination = "C:\Build\ApplicationBackups\"
	[string]   $BackupFileName = $(Get-VXDate -Time) + $BackupExtension
	[String[]] $GraphicExcludes = @("*.jpg","*.png","*.gif","*.psd")
	[String[]] $MusicExcludes = @("*.ogg","*.mp3","*.wav")
	[String[]] $LogExcludes = @("*.log","*.txt","*.InstallLog")
	[String[]] $MiscExcludes = @(,"*.configOLD","*.old","*.bak","*.rb","*.markdown","*.git*","*.gemspec")
	[String[]] $VideoExcludes = @("*.mov","*.swf")
	[String[]] $Exclude = $VideoExcludes + $GraphicExcludes + $MusicExcludes + $LogExcludes + $MiscExcludes
	
	# Work on the destination directory.
	if ( [string]::IsNullOrEmpty($Destination) )
	{
		$Destination = $DefaultDestination
	}
	
	$Destination += "$Environment\$Application\$(Get-VXDate -Date)"
	
	if ( -not [System.IO.Directory]::Exists($Destination) )
	{
		Write-Verbose "The destination directory does not exist."
		try
		{
			Write-Verbose "Creating the destination directory: $Destination"
			[System.IO.Directory]::CreateDirectory($Destination)
		}
		catch [System.Exception] 
		{
			"There was an issue creating the destination directory!"
		}
	}
	
	# Work on the source directory.
	if ( [string]::IsNullOrEmpty($Source) )
	{
		$Source = $UNCSourcePrefix
	    $Source += $(Get-VXApplicationDeployedLocation -Environment $Environment -Application $Application)
	}
	else
	{
		$Source = $UNCSourcePrefix + $Source
	}
	
	if ( [System.IO.Directory]::Exists($Source) )
	{
		Write-Verbose "The source directory does exist."
	}
	else
	{
		Write-Verbose "The source directory does not exist!: $source"
	}
	
	# Begin processing.
	Write-Verbose "Ready to copy $Source to $Destination!"
	
	Write-Verbose "Getting the files to backup!"
	Write-Verbose "Excluding: $Exclude"
	
	$FilesToArchive = Get-ChildItem -Path $Source -Exclude $Exclude -Recurse -Verbose:$VerbosePreference
	
	Write-Verbose "Archiving..."
	$FilesToArchive | Write-Zip -EntryPathRoot $Source -OutputPath "$Destination\$BackupFileName" -Level 9 -NoClobber -Quiet -OutVariable BackupFile	
	
	Write-Verbose "Done archiving!"
	$BackupFile | gm -MemberType method | select Name | ft -auto
	Write-Verbose "Backup of $Application ($Environment) complete!"
}

<#
function Write-7Zip
{
	[CmdletBinding()]
	param
	(
		[string]   $Source,
		[string]   $Destination,
		[String[]] $Exclude
	)
}
#>

<#
function Add-VXPSDrive
{
	[CmdletBinding()]
	param (
        [string[]] $Environment,
        [string[]] $ComputerName,
    	[System.Management.Automation.PSCredential] $Credential
	)
	
	if ( ($Environment -contains "stage" ) -and ( [string]::IsNullOrEmpty($Credential) ) )
	{
		$Credential = Get-VXCredential -Environment STAGE -Credential $Credential
	}
	
	foreach ( $env in $Environment )
	{
		switch ($env)
		{
			"qa" { Add-VXPSDriveWorker -Environment $env }
			"stage" { Add-VXPSDriveWorker -Environment $env -Credential $Credential }
		}
	}	
}
#>

<#
function Add-VXPSDriveWorker 
{
	[CmdletBinding()]
	param (
		[string] $Environment,
        [System.Management.Automation.PSCredential] $Credential
	)
	
	$ComputerName = Get-VXComputerName -Environment $Environment;
	$Provider = "FileSystem";
	$Scope = "Global";
	
	if ( $Environment -eq "stage" )
	{	
		$Credential = Get-VXCredential -Environment $Environment -Credential $Credential;
	}
	
	foreach ( $computer in $ComputerName )
	{
		$UNCPath = "\\$computer\c`$";
		$regex = '`.';
		$regexReplacement = '';
		$formattedComputerName = $computer -replace $regex,$regexReplacement;
		
		if ( $Credential )
		{
			New-PSDrive -Scope $Scope -PSProvider $Provider -Name $formattedComputerName -Root $UNCPath -Credential $Credential;
		} 
		else
		{
			New-PSDrive -Scope $Scope -PSProvider $Provider -Name $formattedComputerName -Root $UNCPath;
		}
	}
}
#>

<#
function Remove-VXPSDriveWorker 
{
	[CmdletBinding()]
	param (
		[string] $Environment
	)
	
	$ComputerName = Get-VXComputerName -Environment $Environment;
	$Scope = "Global";
	
	foreach ( $computer in $ComputerName )
	{
		$UNCPath = "\\$computer\c`$";
		$regex = '`.';
		$regexReplacement = '';
		$formattedComputerName = $computer -replace $regex,$regexReplacement;
		
		Remove-PSDrive -Scope $Scope -Name $formattedComputerName;
	}
}
#>

Export-ModuleMember Get-VXService
Export-ModuleMember Stop-VXService
Export-ModuleMember Start-VXService
Export-ModuleMember Get-VXApplicationDeployedLocation
Export-ModuleMember Backup-VXApplicationWorker
