﻿##########################
## Get the latest code. ##
##########################
if ( -not ( Get-Module PSCX ) )
{
	Import-Module PSCX
}

if ( Get-Module Veritix )
{
	Remove-Module Veritix
}

Import-Module Veritix

##########################
##### Test Functions #####
##########################

function Test-Get-VxServiceNames
{
    Write-Host -Background Green -Foreground White "Testing:" -NoNewline
    Write-Host -Background Blue -Foreground White " Get-VxServiceNames"
    Get-VxServiceNames -Verbose
    Write-Output "Done!"
    Write-Output ""
}

function Test-Get-VxComputerName-QA
{
    Write-Host -Background Green -Foreground White "Testing:" -NoNewline
    Write-Host -Background Blue -Foreground White " Get-VxComputerName QA"
    Get-VxComputerName -Environment QA -Verbose
    Write-Output "Done!"
    Write-Output ""
}

function Test-Get-VxComputerName-STAGE
{
    Write-Host -Background Green -Foreground White "Testing:" -NoNewline
    Write-Host -Background Blue -Foreground White " Get-VxComputerName STAGE"
    Get-VxComputerName -Environment STAGE -Verbose
    Write-Output "Done!"
    Write-Output ""
}

function Test-Get-VxComputerName-BOTH
{
    Write-Host -Background Green -Foreground White "Testing:" -NoNewline
    Write-Host -Background Blue -Foreground White " Get-VxComputerName BOTH"
    Get-VxComputerName -Environment STAGE,QA -Verbose
    Write-Output "Done!"
    Write-Output ""
}
    
function Test-Get-VXService-QA
{
    Write-Host -Background Green -Foreground White "Testing:" -NoNewline
    Write-Host -Background Blue -Foreground White " Get-VXService QA"
    Get-VXService -Environment QA -Verbose
    Write-Output "Done!"
    Write-Output ""
}

function Test-Get-VXService-STAGE
{
    Write-Host -Background Green -Foreground White "Testing:" -NoNewline
    Write-Host -Background Blue -Foreground White " Get-VXService STAGE"
    Get-VXService -Environment STAGE -Verbose
    Write-Output "Done!"
    Write-Output ""
}

function Test-Get-VXService-BOTH-No-Credential
{
    Write-Host -Background Green -Foreground White "Testing:" -NoNewline
    Write-Host -Background Blue -Foreground White " Get-VXService BOTH"
    Get-VXService -Environment QA -Verbose
    Get-VXService -Environment STAGE -Verbose
    Write-Output "Done!"
    Write-Output ""
}

function Test-Get-VXService-QA-DefaultSelectSortAndFormat
{
    Write-Host -Background Green -Foreground White "Testing:" -NoNewline
    Write-Host -Background Blue -Foreground White " Get-VXService QA DefaultSelectSortAndFormat"
    Get-VXService -Environment QA -Verbose -DefaultSelectSortAndFormat
    Write-Output "Done!"
    Write-Output ""
}

function Test-Get-VXCredential
{
    Write-Host -Background Green -Foreground White "Testing:" -NoNewline
    Write-Host -Background Blue -Foreground White " Get-VXCredential"
    Get-VXCredential -Environment STAGE -Verbose
    Write-Output "Done!"
    Write-Output ""
}

function Test-Start-VXService-QA
{
    Write-Host -Background Green -Foreground White "Testing:" -NoNewline
    Write-Host -Background Blue -Foreground White " Start-VXService: QA"
    Start-VXService -Environment QA -Verbose
    Write-Output "Done!"
    Write-Output ""
}

function Test-Stop-VXService-QA
{
    Write-Host -Background Green -Foreground White "Testing:" -NoNewline
    Write-Host -Background Blue -Foreground White " Stop-VXService: QA"
    Stop-VXService -Environment QA -Verbose
    Write-Output "Done!"
    Write-Output ""
}

function Test-Start-VXService-QA-Single-Serivce-VaScheduler
{
    Write-Host -Background Green -Foreground White "Testing:" -NoNewline
    Write-Host -Background Blue -Foreground White " Start-VXService-Single-Serivce-VaScheduler: QA"
    Start-VXService -Environment QA -Service *VAScheduler* -Verbose
    Write-Output "Done!"
    Write-Output ""
}

function Test-Stop-VXService-QA-Single-Serivce-VaScheduler
{
    Write-Host -Background Green -Foreground White "Testing:" -NoNewline
    Write-Host -Background Blue -Foreground White " Stop-VXService-Single-Service-VaScheduler: QA"
    Stop-VXService -Environment QA -Service *VAScheduler* -Verbose
    Write-Output "Done!"
    Write-Output ""
}

function Test-Start-VXService-STAGE
{
	[CmdletBinding()]
	param()
    Write-Host -Background Green -Foreground White "Testing:" -NoNewline
    Write-Host -Background Blue -Foreground White " Start-VXService: STAGE"
    Start-VXService -Environment STAGE -Verbose
    Write-Output "Done!"
    Write-Output ""
}

function Test-Stop-VXService-STAGE
{
	[CmdletBinding()]
	param()
    Write-Host -Background Green -Foreground White "Testing:" -NoNewline
    Write-Host -Background Blue -Foreground White " Stop-VXService: STAGE"
    Stop-VXService -Environment STAGE -Verbose
    Write-Output "Done!"
    Write-Output ""
}

function Test-Restart-VXService-QA
{
	[CmdletBinding()]
	param()
    Write-Host -Background Green -Foreground White "Testing:" -NoNewline
    Write-Host -Background Blue -Foreground White " Restart-VXService: STAGE"
    Restart-VXService -Environment QA -Verbose -ShowServiceStatusAfterComplete
    Write-Output "Done!"
    Write-Output ""
}

function Test-Restart-VXService-STAGE
{
	[CmdletBinding()]
	param()
    Write-Host -Background Green -Foreground White "Testing:" -NoNewline
    Write-Host -Background Blue -Foreground White " Restart-VXService: STAGE"
    Restart-VXService -Environment STAGE -Verbose
    Write-Output "Done!"
    Write-Output ""
}

<#
function Test-Add-VXPSDriveWorker-QA
{
	[CmdletBinding()]
	param()
	
	$Environment = "QA";
	$FunctionName = "Add-VXPSDriveWorker";
	
    Write-Host -Background Green -Foreground White "Testing:" -NoNewline
    Write-Host -Background Blue -Foreground White " $FunctionName: $Environment"
	
    Add-VXPSDriveWorker -Environment $Environment -Verbose
	
    Write-Output "Done!`n"
}
#>

<#
function Test-Add-VXPSDriveWorker-STAGE
{
	[CmdletBinding()]
	param()
	
	$Environment = "STAGE";
	$FunctionName = "Add-VXPSDriveWorker";
	
    Write-Host -Background Green -Foreground White "Testing:" -NoNewline
    Write-Host -Background Blue -Foreground White " $FunctionName: $Environment"
	
    Add-VXPSDriveWorker -Environment $Environment -Verbose
	
    Write-Output "Done!`n"
}
#>

<#
function Test-Add-VXPSDrive-QA
{
	[CmdletBinding()]
	param()
	
	$Environment = "QA";
	$FunctionName = "Add-VXPSDriveWorker";
	
    Write-Host -Background Green -Foreground White "Testing:" -NoNewline
    Write-Host -Background Blue -Foreground White " $FunctionName: $Environment"
	
    Add-VXPSDrive -Environment $Environment -Verbose
	
    Write-Output "Done!`n"
}
#>

<#
function Test-Remove-VXPSDriveWorker-QA
{
	[CmdletBinding()]
	param()
	
	$Environment = "QA";
	$FunctionName = "Remove-VXPSDriveWorker";
	
    Write-Host -Background Green -Foreground White "Testing:" -NoNewline
    Write-Host -Background Blue -Foreground White " $FunctionName: $Environment"
	
    Remove-VXPSDriveWorker -Environment $Environment -Verbose
	
    Write-Output "Done!`n"
}
#>

function Test-Get-VXApplicationDeployedLocation-QA-VAScheduler
{
	[CmdletBinding()]
	param()
	
	$Environment = "QA";
	$FunctionName = "Get-VXApplicationDeployedLocation";
	$Application = "VAScheduler";
	
    Write-Host -Background Green -Foreground White "Testing:" -NoNewline
    Write-Host -Background Blue -Foreground White " $FunctionName: $Environment"
	
    Get-VXApplicationDeployedLocation -Environment $Environment -Application $Application -Verbose
	
    Write-Output "Done!`n"
}

function Test-Get-VXApplicationDeployedLocation-QA-FlashSeats
{
	[CmdletBinding()]
	param()
	
	$Environment = "QA";
	$FunctionName = "Get-VXApplicationDeployedLocation";
	$Application = "FlashSeats";
	
    Write-Host -Background Green -Foreground White "Testing:" -NoNewline
    Write-Host -Background Blue -Foreground White " $FunctionName`: $Environment"
	
    Get-VXApplicationDeployedLocation -Environment $Environment -Application $Application -Verbose
	
    Write-Output "Done!`n"
}

function Test-Backup-VXApplicationWorker-QA-VAScheduler
{
	[CmdletBinding()]
	param()
	
	$Environment = "QA";
	$FunctionName = "Backup-VXApplicationWorker";
	$Application = "VAScheduler";
	
    Write-Host -Background Green -Foreground White "Testing:" -NoNewline
    Write-Host -Background Blue -Foreground White " $FunctionName: $Environment"
	
    Backup-VXApplicationWorker -Environment $Environment -ComputerName dalboqa01 -Application $Application -Verbose
	
    Write-Output "Done!`n"
}

##########################
####### RUN TESTS ########omputerN
##########################

#Test-Get-VxServiceNames
#Test-Get-VxComputerName-QA
#Test-Get-VxComputerName-STAGE
#Test-Get-VxComputerName-BOTH
#Test-Get-VXService-QA
#Test-Get-VXService-STAGE
#Test-Get-VXService-BOTH-No-Credential
#Test-Get-VXService-QA-DefaultSelectSortAndFormat
#Test-Get-VXService-QA-Sorted
#Test-Get-VXService-QA-Sorted-and-Selected
#Test-Get-VXCredential
#Test-Start-VXService-QA
#Test-Stop-VXService-QA
#Test-Start-VXService-STAGE
#Test-Stop-VXService-STAGE
#Test-Stop-VXService-QA-Single-Serivce-VaScheduler
#Test-Start-VXService-QA-Single-Serivce-VaScheduler
#Test-Restart-VXService-QA
#Test-Restart-VXService-STAGE
#Test-Add-VXPSDriveWorker-QA
#Test-Add-VXPSDriveWorker-STAGE
#Test-Remove-VXPSDriveWorker-QA
#Test-Add-VXPSDriveWorker-STAGE
#Test-Add-VXPSDrive-QA
#Test-Get-VXApplicationDeployedLocation-QA-VAScheduler
Test-Get-VXApplicationDeployedLocation-QA-FlashSeats
#Test-Backup-VXApplicationWorker-QA-VAScheduler