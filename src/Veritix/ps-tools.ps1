$cred = Get-Credential "stage\mtarleton";
$codebase = 'C:\Users\michael.tarleton\Desktop\prodcode';
$excludeFiles = @("*.pdb","*.config","*.log4net","*.cfg");
$logDir = 'C:\Users\michael.tarleton\Desktop\';
$logFile = 'StageDeploy.log';
$codebase = '\\dalbuild02\c$\build\Packaged\Trunk';




## Find product versions and names ##
Invoke-Command { gci c:\inetpub\wwwroot\ -recurse -filter "*dll" | Select-Object -Property FullName,@{Label="ProductName";Expression={$_.VersionInfo.ProductName}},@{Label="ProductVersion";Expression={$_.VersionInfo.ProductVersion}} | Where-Object { $_.ProductVersion -like "3.39.*" } | Sort-Object ProductName | ft FullName,ProductName -AutoSize -GroupBy ProductName } -ComputerName dalqaapp01 
Invoke-Command { gci c:\veritix\ -recurse -filter "*dll" | Select-Object -Property FullName,@{Label="ProductName";Expression = { $_.VersionInfo.ProductName }},@{Label="ProductVersion";Expression = { $_.VersionInfo.ProductVersion } } | Where-Object { $_.ProductVersion -like "3.39.*" } | Sort-Object ProductName } -ComputerName dalapi01,dalapi02 | ft PSComputerName,FullName,ProductName,ProductVersion


## QA ##
Invoke-Command -ComputerName (Get-Content H:\PowerShell\qa-server-list.txt) { gci "c:\veritix\","C:\VA","C:\inetpub" -recurse -filter "*.dll" | Select-Object VersionInfo | Where-Object { $_.ProductVersion -LIKE "3.39.*" -OR $_.ProductVersion -EQ "1.0.0.0" } | Sort-Object ProductName,ProductVersion,FileName,PSComputerName } | ft ProductName,ProductVersion,PSComputerName,FileName


### STAGE ###
Invoke-Command -Credential $stageCredential -ComputerName (Get-Content H:\PowerShell\stage-server-list.txt) { gci "c:\veritix\","C:\VA","C:\inetpub" -recurse -filter "*.dll" | Select-Object VersionInfo | Where-Object { $_.ProductVersion -LIKE "3.39.*" -OR $_.ProductVersion -EQ "1.0.0.0" } | Sort-Object ProductName,ProductVersion,FileName,PSComputerName } | ft ProductName,ProductVersion,PSComputerName,FileName

#### Get file versions from the packaged build server. ####
Start-Job -Name GetBuildFileVersions -ScriptBlock { Get-ChildItem -Path C:\build\Packaged\Trunk\x64 -Recurse -Filter "*.dll" | Select -ExpandProperty VersionInfo | Select ProductName,ProductVersion -Unique | Where { .ProductVersion -EQ "1.0.0.0" -OR $_.ProductVersion -LIKE "3.39.*" } | Sort ProductName,ProductVersion | Format-Table -AutoSize -GroupBy ProductName }
dalstage02,01 OTSServers





#### Stop Services ####
##### QA #####
Start-Job -Name StopServices { Get-WmiObject -ComputerName (gc C:\Users\michael.tarleton\Documents\PowerShell\qa-server-list.txt) -Query "SELECT * FROM win32_service WHERE Name LIKE '%va%scheduler%' OR Name LIKE '%ims%' OR Name LIKE '%vxsh%'" | Invoke-WmiMethod -Name StopService |Select ReturnValue | Sort ReturnValue | Format-Table -AutoSize | Out-File $logDir$logFile -Append }
##### STAGE #####
Start-Job -Name StopServices { Get-WmiObject -Credential $cred -ComputerName (gc H:\powershell\stage-server-list.txt) -Query "SELECT * FROM win32_service WHERE Name LIKE '%va%scheduler%' OR Name LIKE '%ims%' OR Name LIKE '%vxsh%'" | Invoke-WmiMethod -Name StopService | Select ReturnValue | Sort ReturnValue | Format-Table -AutoSize | Out-File $logDir$logFile -Append }

#### Start Services ####
##### QA #####
Start-Job -Name StartServices { Get-WmiObject -ComputerName (gc C:\Users\michael.tarleton\Documents\PowerShell\qa-server-list.txt) -Query "SELECT * FROM win32_service WHERE Name LIKE '%va%scheduler%' OR Name LIKE '%ims%' OR Name LIKE '%vxsh%'" | Invoke-WmiMethod -Name StartService |Select ReturnValue | Sort ReturnValue | Format-Table -AutoSize | Out-File $logDir$logFile -Append }
##### STAGE #####
Start-Job -Name StartServices { Get-WmiObject -Credential $cred -ComputerName (gc H:\powershell\stage-server-list.txt) -Query "SELECT * FROM win32_service WHERE Name LIKE '%va%scheduler%' OR Name LIKE '%ims%' OR Name LIKE '%vxsh%'" | Invoke-WmiMethod -Name StartService | Select ReturnValue | Sort ReturnValue | Format-Table -AutoSize | Out-File $logDir$logFile -Append }

#### Select Services ####
##### QA #####
Start-Job -Name SelectServices { Get-WmiObject -ComputerName (gc C:\Users\michael.tarleton\Documents\PowerShell\qa-server-list.txt) -Query "SELECT * FROM win32_service WHERE Name LIKE '%va%scheduler%' OR Name LIKE '%ims%' OR Name LIKE '%vxsh%'" | Select State,Name,SystemName | Sort State,Name,SystemName | Format-Table -AutoSize | Out-File $logDir$logFile -Append }
##### STAGE #####
Start-Job -Name SelectServices { Get-WmiObject -Credential $cred -ComputerName (gc H:\powershell\stage-server-list.txt) -Query "SELECT * FROM win32_service WHERE Name LIKE '%va%scheduler%' OR Name LIKE '%ims%' OR Name LIKE '%vxsh%'" | Select State,Name,SystemName | Sort State,Name,SystemName | Format-Table -AutoSize | Out-File $logDir$logFile -Append }







## PUSH FILES ##
#### API ####
###### QA ######
Start-Job -Name PushAPI01 { Copy-Item -Path $codebase\x64\APIServers\API\* -Destination \\dalapi01\c$\veritix\Api\ -Recurse -Verbose -Exclude $excludeFiles }
Start-Job -Name PushAPI02 { Copy-Item -Path $codebase\x64\APIServers\API\* -Destination \\dalapi02\c$\veritix\Api\ -Recurse -Verbose -Exclude $excludeFiles }
###### STAGE ######
Start-Job -Name PushAPI01 { Copy-Item -Path $codebase\x64\APIServers\API\* -Destination Z:\VA\Common\Websites\API\ -Recurse -Verbose -Exclude $excludeFiles }
Start-Job -Name PushAPI02 { Copy-Item -Path $codebase\x64\APIServers\API\* -Destination X:\VA\Common\Websites\API\ -Recurse -Verbose -Exclude $excludeFiles }

#### FS ####
## QA ##
Start-Job -Name PushFS { Copy-Item -Path $codebase\x64\FS\* -Destination \\dalqaapp01\c$\inetpub\wwwroot\f\ -Recurse -Verbose -Exclude $excludeFiles }
## STAGE ##
Start-Job -Name PushFS01 { Copy-Item -Path $codebase\x64\FS\* -Destination Z:\VA\Common\Websites\f -Recurse -Verbose -Exclude $excludeFiles }
Start-Job -Name PushFS02 { Copy-Item -Path $codebase\x64\FS\* -Destination X:\VA\Common\Websites\f -Recurse -Verbose -Exclude $excludeFiles }

#### OTS (VAST.Web) ####
# STAGE #
Start-Job -Name PushOTS01 { Copy-Item -Path $codebase\x64\OTSServers\VAST.Web\* -Destination Z:\VA\Common\Websites\VAST.Web -Recurse -Verbose -Force -Exclude $excludeFiles }
Start-Job -Name PushOTS02 { Copy-Item -Path $codebase\x64\OTSServers\VAST.Web\* -Destination X:\VA\Common\Websites\VAST.Web -Recurse -Verbose -Force -Exclude $excludeFiles }

#### (VAST.Web.VA) ####
# STAGE #
Start-Job -Name PushVASTWebVA01 { Copy-Item -Path $codebase\x64\OTSServers\VAST.Web.VA\* -Destination Z:\VA\Common\Websites\VAST.Web.VA -Recurse -Verbose -Exclude $excludeFiles }
Start-Job -Name PushVASTWebVA02 { Copy-Item -Path $codebase\x64\OTSServers\VAST.Web.VA\* -Destination X:\VA\Common\Websites\VAST.Web.VA -Recurse -Verbose -Exclude $excludeFiles }

#### ClientPortal ####
# STAGE #
Start-Job -Name PushClientPortal01 { Copy-Item -Path $codebase\x64\OTSServers\ClientPortal\* -Destination Z:\VA\Common\Websites\ClientPortal -Recurse -Force -Verbose -Exclude $excludeFiles }
Start-Job -Name PushClientPortal02 { Copy-Item -Path $codebase\x64\OTSServers\ClientPortal\* -Destination X:\VA\Common\Websites\ClientPortal -Recurse -Force -Verbose -Exclude $excludeFiles }

#### CustomerPortal ####
# STAGE #
Start-Job -Name PushCustomerPortal01 { Copy-Item -Path $codebase\x64\OTSServers\CustomerPortal\* -Destination Z:\VA\Common\Websites\CustomerPortal -Recurse -Verbose -Exclude $excludeFiles }
Start-Job -Name PushCustomerPortal02 { Copy-Item -Path $codebase\x64\OTSServers\CustomerPortal\* -Destination X:\VA\Common\Websites\CustomerPortal -Recurse -Verbose -Exclude $excludeFiles }

#### SpaceBison ####
# STAGE #
Start-Job -Name PushSpaceBison01 { Copy-Item -Path $codebase\x64\SpaceBison\* -Destination Z:\VA\Common\Websites\SpaceBison.Web -Recurse -Verbose -Exclude $excludeFiles }
Start-Job -Name PushSpaceBison02 { Copy-Item -Path $codebase\x64\SpaceBison\* -Destination X:\VA\Common\Websites\SpaceBison.Web -Recurse -Verbose -Exclude $excludeFiles }

#### Integration ####
# STAGE #
Start-Job -Name PushIntegration01 { Copy-Item -Path $codebase\x64\ProductIntegration\* -Destination Z:\VA\Common\Applications\IntegrationHandlers -Recurse -Verbose -Exclude $excludeFiles }
Start-Job -Name PushIntegration02 { Copy-Item -Path $codebase\x64\ProductIntegration\* -Destination X:\VA\Common\Applications\IntegrationHandlers -Recurse -Verbose -Exclude $excludeFiles }

#### AutoBlocker ####
# STAGE #
Start-Job -Name PushAutoBlocker01 { Copy-Item -Path $codebase\x64\AutoBlocker\* -Destination Z:\VA\Common\Applications\AutoBlocker -Recurse -Verbose -Exclude $excludeFiles }
Start-Job -Name PushAutoBlocker02 { Copy-Item -Path $codebase\x64\AutoBlocker\* -Destination X:\VA\Common\Applications\AutoBlocker -Recurse -Verbose -Exclude $excludeFiles }

#### IMS ####
# STAGE #
Start-Job -Name PushIMS01 { Copy-Item -Path $codebase\x64\IMSServers\Common\IMSServer\* -Destination X:\VA\Common\Applications\IMSServer -Recurse -Verbose -Exclude $excludeFiles }

#### ServiceHost ####
# STAGE #
Start-Job -Name PushServiceHost01 { Copy-Item -Path $codebase\x64\BackOfficeServers\Applications\ServiceHost\* -Destination Z:\VA\Common\Applications\ServiceHost -Recurse -Verbose -Exclude $excludeFiles }
Start-Job -Name PushServiceHost02 { Copy-Item -Path $codebase\x64\BackOfficeServers\Applications\ServiceHost\* -Destination X:\VA\Common\Applications\ServiceHost -Recurse -Verbose -Exclude $excludeFiles }

#### AutoUpdate Applications ####
# STAGE #
Start-Job -Name PushAutoUpdate01 { Copy-Item -Path $codebase\x64\BackOfficeServers\Applications\AutoUpdate\* -Destination Z:\VA\Common\Applications\AutoUpdate -Recurse -Verbose -Exclude $excludeFiles }
Start-Job -Name PushAutoUpdate02 { Copy-Item -Path $codebase\x64\BackOfficeServers\Applications\AutoUpdate\* -Destination Z:\VA\Common\Applications\AutoUpdate -Recurse -Verbose -Exclude $excludeFiles }

#### AutoUpdateRepository Applications ####
# STAGE #
Start-Job -Name PushAutoUpdateRepository01 { Copy-Item -Path $codebase\x64\BackOfficeServers\Applications\AutoUpdateRepository\* -Destination Z:\VA\Common\Applications\AutoUpdateRepository -Recurse -Verbose -Exclude $excludeFiles }
Start-Job -Name PushAutoUpdateRepository02 { Copy-Item -Path $codebase\x64\BackOfficeServers\Applications\AutoUpdateRepository\* -Destination Z:\VA\Common\Applications\AutoUpdateRepository -Recurse -Verbose -Exclude $excludeFiles }

#### IntegrationHandlers Applications ####
# STAGE #
Start-Job -Name PushIntegrationHandlers01 { Copy-Item -Path $codebase\x64\BackOfficeServers\Applications\IntegrationHandlers\* -Destination Z:\VA\Common\Applications\IntegrationHandlers -Recurse -Verbose -Exclude $excludeFiles }
Start-Job -Name PushIntegrationHandlers02 { Copy-Item -Path $codebase\x64\BackOfficeServers\Applications\IntegrationHandlers\* -Destination Z:\VA\Common\Applications\IntegrationHandlers -Recurse -Verbose -Exclude $excludeFiles }

#### ServiceHost Applications ####
# STAGE #
Start-Job -Name PushServiceHost01 { Copy-Item -Path $codebase\x64\BackOfficeServers\Applications\ServiceHost\* -Destination Z:\VA\Common\Applications\ServiceHost -Recurse -Verbose -Exclude $excludeFiles }
Start-Job -Name PushServiceHost02 { Copy-Item -Path $codebase\x64\BackOfficeServers\Applications\ServiceHost\* -Destination Z:\VA\Common\Applications\ServiceHost -Recurse -Verbose -Exclude $excludeFiles }

#### Client Applications (VAST.Manager.Web) ####
# STAGE #
Start-Job -Name PushVASTManagerWeb01 { Copy-Item -Path $codebase\x64\BackOfficeServers\Clients\VAST.Manager.Web\* -Destination Z:\VA\Clients\TAMU\Applications\VAST.Manager.Web -Recurse -Verbose -Exclude $excludeFiles }
Start-Job -Name PushVASTManagerWeb02 { Copy-Item -Path $codebase\x64\BackOfficeServers\Clients\VAST.Manager.Web\* -Destination X:\VA\Clients\TAMU\Applications\VAST.Manager.Web -Recurse -Verbose -Exclude $excludeFiles }

#### VAScheduler ####
# QA #
Start-Job -Name PushVAScheduler { Copy-Item -Path $codebase\x64\BackOfficeServers\Applications\VAScheduler\* -Destination \\dalboqa01\c$\VA\Common\Applications\VAScheduler\ -Recurse -Verbose -Exclude $excludeFiles }
# STAGE #
Start-Job -Name PushVAScheduler { Copy-Item -Path $codebase\x64\BackOfficeServers\Applications\VAScheduler\* -Destination Z:\VA\Common\Applications\VAScheduler\ -Recurse -Verbose -Exclude $excludeFiles }










#### Restart Web Servers ####
# QA #
Start-Job -Name RestartWebServers { Get-Content H:\PowerShell\qa-server-list.txt | ForEach { iisreset $_ /restart } }
# STAGE #
Start-Job -Name RestartWebServers { Get-Content H:\PowerShell\stage-server-list.txt | ForEach { iisreset $_ /restart } }
















### MISC ###
#### DALWEBSITE01 - OTS ####
Copy-Item -Path $codebase\x64\OTSServers\VAST.Web\* -Destination \\DALWEBSITE01\c$\VA\Common\Websites\VAST.Web\ -Recurse -Verbose -Exclude *.pdb,*.config,*.log4net
############
    
### FIND AND REPLACE ###
icm -ComputerName (Get-Content -Path "qa-server-list.txt") { 
    Get-ChildItem -Path C:\VA,C:\inetpub,C:\veritx -Filter *.config* -Include Web.Config,*.exe.config -Recurse -ErrorAction SilentlyContinue |
    ForEach-Object {
        $file = $_
        $file.CopyTo("$_.FullName" + ".bak")
        $fileContent = (Get-Content $file.FullName)
        $newFileContent = (
            $fileContent | 
            ForEach-Object {
                #$_ -replace "Vx.ServiceFactory","VA.Framework"
                $_ -replace ",VA.Framework`"",",Vx.ServiceFactory`""
            })
        $newFileContent | Set-Content $file
    #    gc $file | select-string "Vx.ServiceFactory"
    }
}

icm -ComputerName (Get-Content -Path "qa-server-list.txt") { 
#Get-ChildItem -Path C:\VA,C:\inetpub,C:\veritx -Filter *.config* -Include Web.Config,*.exe.config -Recurse | Select-String "VA.Framework"
Get-ChildItem -Path C:\VA,C:\inetpub,C:\veritx -Filter *.config* -Include Web.Config,*.exe.config -Recurse -ErrorAction SilentlyContinue | gc | Select-String "VA.Framework"
}




















Write-Output "Pusing API...";
Write-Output "API 01...";
Copy-Item $codebase\x64\APIServers\API\* -Destination Z:\VA\Common\Websites\API\ -Recurse -Verbose -Exclude $excludeFiles | Out-File $logDir$logFile -Append;
Write-Output "API 02...";
Copy-Item -Path $codebase\x64\APIServers\API\* -Destination X:\VA\Common\Websites\API\ -Recurse -Verbose -Exclude $excludeFiles | Out-File $logDir$logFile -Append;
Write-Output "Pusing API Done!";

Write-Output "Pusing FS...";
Write-Output "FS 01...";
Copy-Item -Path $codebase\x64\FS\* -Destination Z:\VA\Common\Websites\f -Recurse -Verbose -Exclude $excludeFiles | Out-File $logDir$logFile -Append;
Write-Output "FS 02...";
Copy-Item -Path $codebase\x64\FS\* -Destination X:\VA\Common\Websites\f -Recurse -Verbose -Exclude $excludeFiles | Out-File $logDir$logFile -Append;
Write-Output "Pusing FS Done!";

Write-Output "Pusing OTS...";
Write-Output "OTS 01...";
Copy-Item -Path $codebase\x64\OTSServers\VAST.Web\* -Destination Z:\VA\Common\Websites\VAST.Web -Recurse -Verbose -Force -Exclude $excludeFiles | Out-File $logDir$logFile -Append; 
Write-Output "OTS 02...";
Copy-Item -Path $codebase\x64\OTSServers\VAST.Web\* -Destination X:\VA\Common\Websites\VAST.Web -Recurse -Verbose -Force -Exclude $excludeFiles | Out-File $logDir$logFile -Append;
Write-Output "Pusing OTS Done!";

Write-Output "Pusing VASTWebVA...";
Write-Output "VASTWebVA 01...";
Copy-Item -Path $codebase\x64\OTSServers\VAST.Web.VA\* -Destination Z:\VA\Common\Websites\VAST.Web.VA -Recurse -Verbose -Exclude $excludeFiles | Out-File $logDir$logFile -Append;
Write-Output "VASTWebVA 01...";
Copy-Item -Path $codebase\x64\OTSServers\VAST.Web.VA\* -Destination X:\VA\Common\Websites\VAST.Web.VA -Recurse -Verbose -Exclude $excludeFiles | Out-File $logDir$logFile -Append;
Write-Output "Pusing VASTWebVA Done!";

Write-Output "Pusing ClientPortal...";
Write-Output "ClientPortal 01...";
Copy-Item -Path $codebase\x64\OTSServers\ClientPortal\* -Destination Z:\VA\Common\Websites\ClientPortal -Recurse -Force -Verbose -Exclude $excludeFiles | Out-File $logDir$logFile -Append;
Write-Output "ClientPortal 02...";
Copy-Item -Path $codebase\x64\OTSServers\ClientPortal\* -Destination X:\VA\Common\Websites\ClientPortal -Recurse -Force -Verbose -Exclude $excludeFiles | Out-File $logDir$logFile -Append;
Write-Output "Pusing ClientPortal Done!";

Write-Output "Pusing CustomerPortal...";
Write-Output "CustomerPortal 01...";
Copy-Item -Path $codebase\x64\OTSServers\CustomerPortal\* -Destination Z:\VA\Common\Websites\CustomerPortal -Recurse -Verbose -Exclude $excludeFiles | Out-File $logDir$logFile -Append;
Write-Output "CustomerPortal 02...";
Copy-Item -Path $codebase\x64\OTSServers\CustomerPortal\* -Destination X:\VA\Common\Websites\CustomerPortal -Recurse -Verbose -Exclude $excludeFiles | Out-File $logDir$logFile -Append;
Write-Output "Pusing CustomerPortal Done!";

Write-Output "Pusing SpaceBison...";
Write-Output "SpaceBison 01...";
Copy-Item -Path $codebase\x64\SpaceBison\* -Destination Z:\VA\Common\Websites\SpaceBison.Web -Recurse -Verbose -Exclude $excludeFiles | Out-File $logDir$logFile -Append;
Write-Output "SpaceBison 02...";
Copy-Item -Path $codebase\x64\SpaceBison\* -Destination X:\VA\Common\Websites\SpaceBison.Web -Recurse -Verbose -Exclude $excludeFiles | Out-File $logDir$logFile -Append;
Write-Output "Pusing SpaceBison Done!";

Write-Output "Pusing Integration...";
Write-Output "Integration 01...";
Copy-Item -Path $codebase\x64\ProductIntegration\* -Destination Z:\VA\Common\Applications\IntegrationHandlers -Recurse -Verbose -Exclude $excludeFiles | Out-File $logDir$logFile -Append;
Write-Output "Integration 02...";
Copy-Item -Path $codebase\x64\ProductIntegration\* -Destination X:\VA\Common\Applications\IntegrationHandlers -Recurse -Verbose -Exclude $excludeFiles | Out-File $logDir$logFile -Append;
Write-Output "Pusing Integration Done!";

Write-Output "Pusing AutoBlocker...";
Write-Output "AutoBlocker 01...";
Copy-Item -Path $codebase\x64\AutoBlocker\* -Destination Z:\VA\Common\Applications\AutoBlocker -Recurse -Verbose -Exclude $excludeFiles | Out-File $logDir$logFile -Append;
Write-Output "AutoBlocker 02...";
Copy-Item -Path $codebase\x64\AutoBlocker\* -Destination X:\VA\Common\Applications\AutoBlocker -Recurse -Verbose -Exclude $excludeFiles | Out-File $logDir$logFile -Append;
Write-Output "Pusing AutoBlocker Done!";

Write-Output "Pusing IMS...";
Write-Output "IMS 01...";
Copy-Item -Path $codebase\x64\IMSServers\Common\IMSServer\* -Destination X:\VA\Common\Applications\IMSServer -Recurse -Verbose -Exclude $excludeFiles | Out-File $logDir$logFile -Append;
Write-Output "Pusing IMS Done!";

Write-Output "Pusing ServiceHost...";
Write-Output "ServiceHost 01...";
Copy-Item -Path $codebase\x64\BackOfficeServers\Applications\ServiceHost\* -Destination Z:\VA\Common\Applications\ServiceHost -Recurse -Verbose -Exclude $excludeFiles | Out-File $logDir$logFile -Append;
Write-Output "ServiceHost 02...";
Copy-Item -Path $codebase\x64\BackOfficeServers\Applications\ServiceHost\* -Destination X:\VA\Common\Applications\ServiceHost -Recurse -Verbose -Exclude $excludeFiles | Out-File $logDir$logFile -Append;
Write-Output "Pusing ServiceHost Done!";

Write-Output "Pusing AutoUpdate0...";
Write-Output "AutoUpdate0 01...";
Copy-Item -Path $codebase\x64\BackOfficeServers\Applications\AutoUpdate\* -Destination Z:\VA\Common\Applications\AutoUpdate -Recurse -Verbose -Exclude $excludeFiles | Out-File $logDir$logFile -Append;
Write-Output "AutoUpdate0 02...";
Copy-Item -Path $codebase\x64\BackOfficeServers\Applications\AutoUpdate\* -Destination Z:\VA\Common\Applications\AutoUpdate -Recurse -Verbose -Exclude $excludeFiles | Out-File $logDir$logFile -Append;
Write-Output "Pusing AutoUpdate0 Done!";

Write-Output "Pusing AutoUpdateRepository...";
Write-Output "AutoUpdateRepository 01...";
Copy-Item -Path $codebase\x64\BackOfficeServers\Applications\AutoUpdateRepository\* -Destination Z:\VA\Common\Applications\AutoUpdateRepository -Recurse -Verbose -Exclude $excludeFiles | Out-File $logDir$logFile -Append;
Write-Output "AutoUpdateRepository 02...";
Copy-Item -Path $codebase\x64\BackOfficeServers\Applications\AutoUpdateRepository\* -Destination Z:\VA\Common\Applications\AutoUpdateRepository -Recurse -Verbose -Exclude $excludeFiles | Out-File $logDir$logFile -Append;
Write-Output "Pusing AutoUpdateRepository Done!";

Write-Output "Pusing IntegrationHandlers...";
Write-Output "IntegrationHandlers 01...";
Copy-Item -Path $codebase\x64\BackOfficeServers\Applications\IntegrationHandlers\* -Destination Z:\VA\Common\Applications\IntegrationHandlers -Recurse -Verbose -Exclude $excludeFiles | Out-File $logDir$logFile -Append;
Write-Output "IntegrationHandlers 02...";
Copy-Item -Path $codebase\x64\BackOfficeServers\Applications\IntegrationHandlers\* -Destination Z:\VA\Common\Applications\IntegrationHandlers -Recurse -Verbose -Exclude $excludeFiles | Out-File $logDir$logFile -Append;
Write-Output "Pusing IntegrationHandlers Done!";

Write-Output "Pusing ServiceHost...";
Write-Output "ServiceHost 01...";
Copy-Item -Path $codebase\x64\BackOfficeServers\Applications\ServiceHost\* -Destination Z:\VA\Common\Applications\ServiceHost -Recurse -Verbose -Exclude $excludeFiles | Out-File $logDir$logFile -Append;
Write-Output "ServiceHost 02...";
Copy-Item -Path $codebase\x64\BackOfficeServers\Applications\ServiceHost\* -Destination Z:\VA\Common\Applications\ServiceHost -Recurse -Verbose -Exclude $excludeFiles | Out-File $logDir$logFile -Append;
Write-Output "Pusing ServiceHost Done!";

Write-Output "Pusing VASTManagerWeb...";
Write-Output "VASTManagerWeb 01...";
Copy-Item -Path $codebase\x64\BackOfficeServers\Clients\VAST.Manager.Web\* -Destination Z:\VA\Clients\TAMU\Applications\VAST.Manager.Web -Recurse -Verbose -Exclude $excludeFiles | Out-File $logDir$logFile -Append;
Write-Output "VASTManagerWeb 02...";
Copy-Item -Path $codebase\x64\BackOfficeServers\Clients\VAST.Manager.Web\* -Destination X:\VA\Clients\TAMU\Applications\VAST.Manager.Web -Recurse -Verbose -Exclude $excludeFiles | Out-File $logDir$logFile -Append;
Write-Output "Pusing VASTManagerWeb Done!";

Write-Output "Pusing VAScheduler...";
Write-Output "VAScheduler 01...";
Copy-Item -Path $codebase\x64\BackOfficeServers\Applications\VAScheduler\* -Destination X:\VA\Common\Applications\VAScheduler\ -Recurse -Verbose -Exclude $excludeFiles | Out-File $logDir$logFile -Append;
Write-Output "VAScheduler 02...";
Copy-Item -Path $codebase\x64\BackOfficeServers\Applications\VAScheduler\* -Destination Z:\VA\Common\Applications\VAScheduler\ -Recurse -Verbose -Exclude $excludeFiles | Out-File $logDir$logFile -Append;
Write-Output "Pusing VAScheduler Done!";

Write-Output "Pusing Vx.ImageContentServer...";
Write-Output "Vx.ImageContentServer 01...";
Copy-Item -Path $codebase\x64\Vx.ImageContentServer\bin\* -Destination Z:\VA\Common\Websites\Vx.ImageContentServer\bin\ -Recurse -Verbose -Exclude $excludeFiles | Out-File $logDir$logFile -Append;
Write-Output "Vx.ImageContentServer 02...";
Copy-Item -Path $codebase\x64\Vx.ImageContentServer\bin\* -Destination X:\VA\Common\Websites\Vx.ImageContentServer\bin\ -Recurse -Verbose -Exclude $excludeFiles | Out-File $logDir$logFile -Append;
Write-Output "Pusing VAScheduler Done!";

Write-Output "Pusing OLAP...";
Write-Output "OLAP 01...";
Copy-Item -Path $codebase\x64\OLAPServers\VAOLAPServices\* -Destination Z:\VA\Common\Applications\OLAP\ -Recurse -Verbose -Exclude $excludeFiles | Out-File $logDir$logFile -Append;
Write-Output "Pusing VAScheduler Done!";



### Backup All configs ###
icm -cred $cred -computer (gc C:\Users\michael.tarleton\Documents\PowerShell\stage-server-list.txt) { gci c:\va,c:\inetpub,c:\vx
-r -filter *.log4net -ea silentlycontinue | select Directory,Name,FullName | %{$t = $_.Directory.FullName;$t += ".";$t+=$_.Name;$t += ".bak"; copy-ite
m -path $_.FullName -destination $t -v} }


### Backup LOG4NET configs (log4net) ###
icm -cred $cred -computer (gc C:\Users\michael.tarleton\Documents\PowerShell\stage-server-list.txt) { gci c:\va,c:\inetpub,c:\vx
-r -filter *.log4net -ea silentlycontinue | select Directory,Name,FullName | %{$t = $_.Directory.FullName;$t += ".";$t+=$_.Name;$t += ".bak"; copy-ite
m -path $_.FullName -destination $t -v} }



### Search and replace in files ###
function
{
	param(
	)
}
icm -cred $cred -computer (gc C:\Users\michael.tarleton\Documents\PowerShell\stage-server-list.txt) { gci c:\va,c:\inetpub,c:\vx
-r -filter *.log4net -ea silentlycontinue | select FullName | %{ $content = gc $_.FullName ; $newContent = $content -replace '<IpAddress value=["0-9.]
*/>','<IpAddress value="stagelogrelay-vip.corp.veritix.com"/>'; $newContent = $newContent -replace '<level value=["a-zA-Z]*/>','<level value="INFO"/>'
 ; Set-Content -Value $newContent -Path $_.FullName}}







### Search file content with color ###
PS C:\vxrepos\Trunk> icm -cred $cred -computer (gc C:\Users\michael.tarleton\Documents\PowerShell\stage-server-list.txt) { gci -path C:\va,c:\vx,c:\in
etpub -filter Web.Config -Recurse -ea SilentlyContinue | select Directory,Name,Fullname | %{write-host ""; write-host -back blue (hostname); write-hos
t -back red $_.FullName; gc $_.FullName | select-string -pattern "192.[0-9]*.[0-9]*.[0-9]*" | write-host } }