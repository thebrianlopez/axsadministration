function Update-VXRepository
{
    param(
        [Parameter(Mandatory=$True)]
        [string]$Branch,
        [Parameter(Mandatory=$True)]
        [string]$Directory,
        [Parameter(Mandatory=$True)]
        [string]$RepoClient
    )
    
    Write-Debug $Branch
       
    cd $Directory
    
    if( $RepoClient -eq "git" )
    {
        git reset --hard -q
        git clean -fd
        git fetch 
        git checkout $Branch
    } 
    elseif( $RepoClient -eq "hg" )
    {
        hg revert --all
        hg purge --all
        hg pull
        hg up $Branch
    }
}

function Get-VXRepositoryLabel
{    
    param(
        [Parameter(Mandatory=$True)]
        [string]$Directory,
        [Parameter(Mandatory=$True)]
        [string]$RepoClient
    )
    
    Write-Debug $Directory
    Write-Debug $RepoClient
    
    cd $Directory
    
    if( $RepoClient -eq "git" )
    {
        $formattedLabel = (git log -1 --pretty=format:%h%d)
    } 
    elseif( $RepoClient -eq "hg" )
    {hg log -l1 --template "{branch}-{node|short}"
        $formattedLabel = (hg log -l1 --template "{branch}-{node|short}")
    }
    
    Write-Debug $formattedLabel
    
    return $formattedLabel
}

function Format-VXRepositoryLabel
{
    param (
        [Parameter(Mandatory=$True)]
        [string]$Branch,
        [Parameter(Mandatory=$True)]
        [string]$Directory,
        [Parameter(Mandatory=$True)]
        [string]$RepoClient
    )
    
    Write-Debug $Branch
    Write-Debug $Directory
    
    try
    {
        Update-VXRepository -Branch $Branch -Directory $Directory -RepoClient $RepoClient -ErrorAction SilentlyContinue
        return Get-VXRepositoryLabel -Directory $Directory -RepoClient $RepoClient
    }
    catch
    {
        Write-Host "There was an error updating the repository."
    }
    
    return ''
}