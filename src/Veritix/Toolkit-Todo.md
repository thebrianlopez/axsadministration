Powershell Environmental Toolkit
================================

Services
--------
* Get
	`gwmi -class win32_service`
* Stop
	`gwmi -class win32_service` | iwmi -Name StopService
* Start
	`gwmi -class win32_service` | iwmi -Name StartService