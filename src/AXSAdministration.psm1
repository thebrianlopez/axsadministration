$global:scriptpath = $MyInvocation.MyCommand.Definition
$global:scriptname = $MyInvocation.MyCommand.Name
$global:thismoddir = (split-path -parent $global:scriptpath)

# ServerManager
# Get-Module -ListAvailable | where {$_.Name -eq "ServerManager"} | Import-Module # This works on Windows Server 2008 R2

# Active Directory
#Get-Module -ListAvailable | where {$_.Name -eq "ActiveDirectory"} | Import-Module # This works on Windows Server 2008 R2

. "$global:thismoddir\AXS\Aliases.ps1"
. "$global:thismoddir\AXS\Get-PendingReboot.ps1"
. "$global:thismoddir\AXS\Invoke-CustomZip.ps1"
. "$global:thismoddir\AXS\Invoke-CustomNuget.ps1"
. "$global:thismoddir\AXS\Start-CustomProcess.ps1"
. "$global:thismoddir\AXS\Invoke-CustomRecycleIISLogs.ps1"
. "$global:thismoddir\AXS\Invoke-CustomS3Upload.ps1"
. "$global:thismoddir\AXS\Invoke-CustomUpdateEBApplication.ps1"

function New-CustomNuSpec{
  [CmdletBinding()]
  param(
        [parameter(Mandatory=$false,Position=0)][io.directoryinfo]$basepath="$(get-location)",
        [parameter(Mandatory=$false,Position=0)][io.directoryinfo][alias("o")]$OutputDirectory="$(get-location)",
        [parameter(Mandatory=$false)][string]$id="my_cool_module",
        [parameter(Mandatory=$false)][string]$version="1.0.2014",
        [parameter(Mandatory=$false)][string]$authors="$env:username",
        [parameter(Mandatory=$false)][string]$owners="$env:username",
        [parameter(Mandatory=$false)][string]$projectUrl="http://company.com/wiki/index.php/$id",
        [parameter(Mandatory=$false)][string]$licenseUrl=$projectUrl,
        [parameter(Mandatory=$false)][string]$iconUrl=$projectUrl,
        [parameter(Mandatory=$false)][string]$requireLicenseAcceptance="false",
        [parameter(Mandatory=$false)][string]$description="Standard PowerShell Module",
        [parameter(Mandatory=$false)][string]$releaseNotes="CI build created $((get-date).DateTime) $(([TimeZoneInfo]::Local).DisplayName)",
        [parameter(Mandatory=$false)][string]$copyright="false",
        [parameter(Mandatory=$false)][io.fileinfo]$nuget="nuget.exe",
        [parameter(Mandatory=$false)][io.fileinfo[]]$files,
        [parameter(Mandatory=$false)][string]$tags="false"
    )

  if(-not($nuget.Exists)){ write-error "Unable to find $nuget" -ea Stop; throw;}

  . $nuget.FullName spec "$($OutputDirectory.FullName)\Package" -f

  [io.fileinfo]$nuspecfile = "$OutputDirectory\Package.nuspec"

  write-verbose "Using the file $nuspecfile"
  [xml]$newfile = Get-Content "$nuspecfile"
  $newfile.package.ChildNodes.Item(0).id=$id
  $newfile.package.ChildNodes.Item(0).version=$version
  $newfile.package.ChildNodes.Item(0).authors=$authors
  $newfile.package.ChildNodes.Item(0).owners=$owners
  $newfile.package.ChildNodes.Item(0).licenseUrl=$licenseUrl
  $newfile.package.ChildNodes.Item(0).projectUrl=$projectUrl
  $newfile.package.ChildNodes.Item(0).iconUrl=$iconUrl
  $newfile.package.ChildNodes.Item(0).requireLicenseAcceptance=$requireLicenseAcceptance
  $newfile.package.ChildNodes.Item(0).description=$description
  $newfile.package.ChildNodes.Item(0).releaseNotes=$releaseNotes
  $newfile.package.ChildNodes.Item(0).copyright=$copyright
  $newfile.package.ChildNodes.Item(0).tags=$tags

  $newfile.package.metadata.RemoveChild($newfile.package.metadata.dependencies) | out-null
  
  # create the files nodes
  if($files.count -gt 0){
    $items = $newfile.CreateElement("files")

    foreach($file in $files){
      write-host "Adding the file $($file.Name)"
      $item = $newfile.CreateElement("file")
      $item.SetAttribute("src","$file")
      $items.AppendChild($item)
    }

    $newfile.package.AppendChild($items) | out-null
  }

  # Save File  
  $newfile.Save("$nuspecfile")

  # not sure if we need to pack this as this should only generate the file
  . $nuget.FullName pack $nuspecfile -OutputDirectory $OutputDirectory

}

function Get-CustomRunningServices
{
  param(
	[Parameter(Mandatory=$True)][string]$computername
	)
  # Since we also support Windows Server 2003 instances we need to use get-wmiobject. Else we can use Get-CimInstance as its _much_ faster
  get-wmiobject -computername $computername -Query "select * from win32_service" | ogv
}

function Get-CustomRunningProcess
{
  param(
   [Parameter(Mandatory=$true)]$ComputerName
  )
  get-wmiobject -computername $computername -query "SELECT * FROM Win32_PerfFormattedData_PerfProc_Process" | ogv
}

function Test-Administrator
{  
    $user = [Security.Principal.WindowsIdentity]::GetCurrent();
    (New-Object Security.Principal.WindowsPrincipal $user).IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)  
}

function Get-RemoteFile
{
  param(
	[Parameter(Mandatory=$true)][system.uri]$uri,
	[io.fileinfo]$outfile
  )
  $b="$env:userprofile\Downloads\$([io.path]::GetFileName($uri.LocalPath))"
  (new-object net.webclient).DownloadFile($uri,$b) | iex
  return $b
}

Export-ModuleMember -Function * -Alias *
