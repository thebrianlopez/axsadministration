$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$sut = (Split-Path -Leaf $MyInvocation.MyCommand.Path).Replace(".Tests.",".").Replace(".ps1","")

Describe -Tags "Module" "Importing the module $sut" {

    It "should be able to import the module" { 
        Import-module $here\$sut.psm1 -verbose
        @(get-module -Name $sut).count | should be 1
    }
}