@echo off

@set errorlevel=0
@set solutiondir=%cd%
@set toolsDir=%solutiondir%\.tools
@set nuget=%solutiondir%\.nuget\nuget.exe
@set config=%solutiondir%\.nuget\nuget.config
@set packages=%solutiondir%\packages
@set pester=%solutiondir%\packages\pester
@set outputDir=%solutiondir%\artifacts
@set sourceDir=%solutiondir%\src
@set octoPath=%packages%\octotools\octo.exe
@set psmodulepath=%psmodulepath%;%packages%;%pester%;%pester%\pester
@set path=%path%;%packages%\.nuget
@set GitVersion=%packages%\GitVersion.CommandLine\tools\GitVersion.exe
@set jq=%packages%\jq.CommandLine\tools\jq-win32.exe



:: Step 1: Get version info
set cmd="%GitVersion% | %jq% .NuGetVersion"
FOR /F "tokens=*" %%i IN (' %cmd% ') DO SET version=%%i
echo Version = %version%

:: Step 2: Make sure we are in the soultion directory
pushd %solutiondir%

:: Step 3: Set the project name 
for %%* in (.) do set projectName=%%~nx*

:: Step 4: Confirm the apikey exists
if defined apikey (echo Found apikey) else (echo Unable to find apikey variable && goto noapikey)

:: Step 5: Make sure our artifact exists 
if exist "artifacts\%projectName%*.nupkg" (echo Package created successfully && goto nugetpush) else ( goto failed)

echo %errorlevel%
goto :eof

:: End of script

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Private functions
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:powershell 
set errorlevel=0
set posh= powershell -version %powershellVersion% -noprofile -executionpolicy bypass -command ^ %1
echo Running the command %posh%
%posh%
FOR /F "tokens=*" %%i IN (' %posh% ') DO SET errorlevel=%%i
echo The command %posh% completed with exit code %errorlevel%
goto :eof

:nugetpush 
echo Pushing nuget with api key %apikey%
%nuget% push artifacts\%projectName%*.nupkg -source myget -apikey %apikey% -config %config% -verbosity detailed
goto :eof 

:noapikey
echo Please set the apikey variable to allow nuget pushes
exit 999
goto :eof 

:success 
echo Build succeeded with exit code %errorlevel%
exit /b %errorlevel%
goto :eof 

:nugetrestore
%nuget% "sources" "list" "-configfile" %config% -verbosity detailed

if not exist "%packages%\7-Zip.CommandLine" (
	%nuget% "install" "7-Zip.CommandLine" "-outputdirectory" %packages% "-excludeversion" "-configfile" %config%
)

if not exist "%packages%\octotools" (
	%nuget% "install" "octotools" "-outputdirectory" %packages% "-excludeversion" "-configfile" %config%
)

if not exist "%packages%\pester" (
	%nuget% "install" "pester" "-outputdirectory" %packages% "-excludeversion" "-configfile" %config%
)

if not exist "%packages%\GitVersion.CommandLine" (
	%nuget% "install" "GitVersion.CommandLine" "-outputdirectory" %packages% "-excludeversion" "-configfile" %config%
)

if not exist "%packages%\AXSAdministration" (
	%nuget% "install" "AXSAdministration" "-outputdirectory" %packages% "-excludeversion" "-configfile" %config%
)

if not exist "%packages%\JQ.CommandLine" (
	%nuget% "install" "JQ.CommandLine" "-outputdirectory" %packages% "-excludeversion" "-configfile" %config%
)

if exist "%packages%\pester\tools" (
	ren "%packages%\pester\tools" "pester"
)
goto :eof 

:failed
echo This build failed with exit code %errorlevel%
exit /b 
