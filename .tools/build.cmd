@echo off

@set %errorlevel%=0
@set solutiondir=%cd%
@set toolsDir=%solutiondir%\.tools
@set nuget=%solutiondir%\.nuget\nuget.exe
@set config=%solutiondir%\.nuget\nuget.config
@set packages=%solutiondir%\packages
@set pester=%solutiondir%\packages\pester
@set outputDir=%solutiondir%\artifacts
@set sourceDir=%solutiondir%\src
@set octoPath=%packages%\octotools\octo.exe
@set psmodulepath=%psmodulepath%;%packages%;%pester%;%pester%\pester;%pester%\tools
@set path=%path%;%packages%\.nuget
@set GitVersion=%packages%\GitVersion.CommandLine\tools\GitVersion.exe
@set jq=%packages%\jq.CommandLine\tools\jq-win32.exe

:: End of variable declarations ::::::::::::::::::::::::::::::::::::::::
:: Start script body            ::::::::::::::::::::::::::::::::::::::::

:: Step 0: Check which build param was specified
set powershellVersion=%1
if not defined powershellVersion (set powershellVersion=2.0)

:: Step 1: Restore packages
call:nugetrestore

:: Step 2: Gather versioning info
set cmd="%GitVersion% | %jq% .NuGetVersion"
%GitVersion%
FOR /F "tokens=*" %%i IN (' %cmd% ') DO SET version=%%i
set version=%version:"=%
echo Version is %version%

:: Step 3: update psmodules path to allow importing powershell modules from our downloaded nuget packages
pushd %solutiondir%
for %%* in (.) do set projectName=%%~nx*

:: Step 4: Push readme.md to directory %sourceDir%
copy %solutiondir%\readme.md %sourceDir%\ /y

:: Step 5: Test script module with specified powershell version 
call:powershell  "import-module Pester -verbose;$results = (invoke-pester %sourceDir% -passthru);$results;return $results.FailedCount;"
if not %errorlevel%==0 (goto failed)

:: Step 7: Build the PowerShell Module with specified version
call:powershell ". %toolsDir%\New-PSModulePackage.ps1; New-PSModulePackage -path %sourceDir% -name %projectName% -output %outputDir% -octo %octoPath% -package -version %version% -verbose; return $error.count;"
if not %errorlevel%==0 (goto failed)

:: Step 11: Echo the errorlevel to stdout to notify the build server
echo %errorlevel%
goto :eof
:: End of script

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Private functions
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:powershell 
set errorlevel=0
set posh= powershell -version %powershellVersion% -noprofile -executionpolicy bypass -command ^ %1
echo Running the command %posh%
%posh%
FOR /F "tokens=*" %%i IN (' %posh% ') DO SET errorlevel=%%i
echo The command %posh% completed with exit code %errorlevel%
goto :eof

:nugetpush 
echo Pushing nuget with api key %apikey%
%nuget% push artifacts\%projectName%*.nupkg -source myget -apikey %apikey% -config %config% -verbosity detailed
goto :eof 

:noapikey
echo Please set the apikey variable to allow nuget pushes
exit 999
goto :eof 

:success 
echo Build succeeded with exit code %errorlevel%
exit /b %errorlevel%
goto :eof 

:nugetrestore
%nuget% "sources" "list" "-configfile" %config% -verbosity detailed

if not exist "%packages%\7-Zip.CommandLine" (
	%nuget% "install" "7-Zip.CommandLine" "-outputdirectory" %packages% "-excludeversion" "-configfile" %config%
)

if not exist "%packages%\octotools" (
	%nuget% "install" "octotools" "-outputdirectory" %packages% "-excludeversion" "-configfile" %config%
)

if not exist "%packages%\pester" (
	%nuget% "install" "pester" "-outputdirectory" %packages% "-excludeversion" "-configfile" %config%
)

if not exist "%packages%\GitVersion.CommandLine" (
	%nuget% "install" "GitVersion.CommandLine" "-outputdirectory" %packages% "-excludeversion" "-configfile" %config%
)

if not exist "%packages%\JQ.CommandLine" (
	%nuget% "install" "JQ.CommandLine" "-outputdirectory" %packages% "-excludeversion" "-configfile" %config%
)

if not exist "%packages%\AXSAdministration" (
	%nuget% "install" "AXSAdministration" "-outputdirectory" %packages% "-excludeversion" "-configfile" %config%
)

if exist "%packages%\pester\pester" (
	ren "%packages%\pester\tools" "pester"
)
goto :eof 

:failed
echo This build failed with exit code %errorlevel%
exit /b 
