# AXSAdministration PowerShell Module #

Latest version : ```1.2.3```

 Branch | Build Status | Comments
 :--- | :--- | :---
```dev```| [![Logo][dev-status]](https://ci.appveyor.com/project/thebrianlopez/axsadministration/branch/dev) | ```dev``` branch is what is deployed in the ```dev``` and ```qa``` environments. 
```master```| [![Logo][master-status]](https://ci.appveyor.com/project/thebrianlopez/axsadministration/branch/master) | ```master``` branch should always reflect what is working and validated in the ```dev``` and ```qa``` environments. 

## Installation ##
* See the section below titled **Installing PowerShell Module with Nuget**

## Usage ##
For further details refer to the [documentation]

## Contributing ##
1. Check out the ```dev``` branch

```
git clone git@github.com:Veritix/AXSAdministration.git -b dev
cd AXSAdministration
```

1. Make modifications as needed
1. Validate unit tests run successfully and checkin changes by performing the following:

```
.tools\build.cmd  # Make sure all tests pass
git commit -m "Added xyz"
git push origin dev
```

1. Once commit has been merged into the ```dev``` branch a Nuget Module will be built and versioned as ```axsadministration-1.n.n-unstablennnn```
1. Next step is to test the module in the ```dev``` environment and confirm operation.
1. Once the module is working without issues in the dev environment **only then** we may merge the ```dev``` branch into ```master```. This way we can ensure the master branch always has functioning code that is known to be working in a pre-production environment.

```
git checkout master
git pull origin master
git fetch origin dev
git merge origin dev
git push origin master
```
## Documentation ##
See the [documentation]

## Governance ##
See the [documentation]

## Issues ##
Jira

## Appendix ##

###  Delete IISLogs older than n days###

```powershell
Import-module WebAdministration,AXSAdministration;deleteiislogs -timespan (get-date).AddDays(-3) -verbose ;
```

### Installing PowerShell Module with Nuget ###

1. Open a cmd prompt
1. Run the following command.

```powershell
powershell -exec Unrestricted -nopro -c "iex (new-object net.webclient).downloadstring('https://raw.githubusercontent.com/Veritix/AXSAdministration/master/.tools/nuget-install.ps1?token=AAUDsZETy1xrPg5s_KKDOQlq3jfxZTvtks5YJfcqwA%3D%3D')"
```

[Documentation]: https://axsteam.atlassian.net/wiki/display/OPS/Operations+Home
[master-status]: https://ci.appveyor.com/api/projects/status/tvqlwthtt9nqc7nx/branch/master?svg=true
[qa-status]: https://ci.appveyor.com/api/projects/status/tvqlwthtt9nqc7nx/branch/qa?svg=true
[dev-status]: https://ci.appveyor.com/api/projects/status/tvqlwthtt9nqc7nx/branch/dev?svg=true

