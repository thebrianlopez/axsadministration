# DEV Environment (aka DEV3) #

|Component | URL |
| --- | --- |
| Flash Services | http://dev3-flashservices.corp.veritix.com/admin/services.aspx |



[Ops Issues]: https://axsteam.atlassian.net/secure/RapidBoard.jspa?projectKey=OPS&rapidView=78&view=planning
[SysOps Issues]: https://axsteam.atlassian.net/projects/SYSOPS/issues?&orderby=created+DESC
[Dev Issues]: https://axsteam.atlassian.net/projects/DEVTEAM/issues?&orderby=created+DESC
[Wiki]: https://axsteam.atlassian.net/wiki/discover/all-updates
[Dev Wiki]: https://axsteam.atlassian.net/wiki/display/DEV/
[Ops Wiki]: https://axsteam.atlassian.net/wiki/display/OPS/
[Departments Wiki]: https://axsteam.atlassian.net/wiki/display/FTD/
[#qa3]: https://axsall.slack.com/messages/qa3/
[#dev-general]: https://axsall.slack.com/messages/dev-general/
[#general]: https://axsall.slack.com/messages/general/
[#sysops-private-cle]: https://axsall.slack.com/messages/sysops-private-cle/
[#sysops-private-dal]: https://axsall.slack.com/messages/sysops-private-dal/
[#sysops-private-la]: https://axsall.slack.com/messages/sysops-private-la/
[#ops-sys-engineering]: https://axsall.slack.com/messages/ops-sys-engineering/
[#ops-private-chat]: https://axsall.slack.com/messages/ops-private-chat/
[prtg]: https://monitor.veritix.net/sensors.htm?id=0&filter_status=5
[new relic]: https://rpm.newrelic.com/accounts/686338/applications
[logo]: https://axsteam.atlassian.net/wiki/download/attachments/655361/global.logo?version=1&modificationDate=1464206702147&api=v2
[Veritix Github]: https://github.com/orgs/Veritix/dashboard
[Personal MyGet]: https://www.myget.org/feed/Details/thebrianlopez
[AXS Github]: https://github.com/aegaxs/ 
[Veritix AWS Console]: https://veritix-west-ec2.signin.aws.amazon.com/console
[AXS AWS Console]: https://axsdevops.signin.aws.amazon.com/console
[blue bridge cloud]: https://cloud.bluebridgenetworks.com/
[OpsServer]: http://52.52.79.77/hub
[TeamCity]: http://teamcity.corp.veritix.com:8081/
[Octopus Deploy]: http://octopus.corp.veritix.com/
[Veritix MyGet]: https://www.myget.org/F/veritix/
[Grafana]: https://graphs.axsops.com/grafana/login
[Sumologic]: https://service.us2.sumologic.com/ui/?goto=welcomeAfterPwdChange#section/live_tail
[Todoist]: https://todoist.com/app
[Outlook]: https://outlook.office.com/owa/?path=/mail/inbox
[Calendar]: https://outlook.office.com/owa/?path=/calendar/view/Week
[Personal AppVeyor]: https://ci.appveyor.com/project/thebrianlopez/axsadministration
[Trello]: https://trello.com/b/6d6ErAiQ/engineering-ideas
[Onsale]: https://axsteam.atlassian.net/wiki/display/DEV/On-Sales+Using+Queue-IT
[PagerDuty]: http://outbox.pagerduty.com/
[Sumologic]: https://service.us2.sumologic.com/ui/dashboard.html?f=23640125&t=r
