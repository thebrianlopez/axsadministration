## VMWare Hosts and Guests ##


| IP Address	| Hostname		| Shortname		| Comments|
| ------------- |:-------------:| -------------:| -------:|
|10.176.102.151 |SUP-UCSESX101	| esx1			|# new vmhost running on cisco ucs and nimble storage 
|10.176.102.152 |SUP-UCSESX102	|esx2			|# new vmhost running on cisco ucs and nimble storage 
|10.176.102.153 |SUP-UCSESX103	|esx3			|# new vmhost running on cisco ucs and nimble storage 
|10.176.102.154 |SUP-UCSESX104	|esx4			|# new vmhost running on cisco ucs and nimble storage 
|10.176.110.121 |SUP-ESXAPODa	|--				|# vm hosts containing apod 104,106,109,111
|10.176.110.122 |SUP-ESXAPODb	|--				|# vm hosts containing apod 106,107,108,110,112
|10.176.102.164 |CASUNNESX104	|--				|# host runing bo103, casunnbo110, casunnxenpoc, mon105, rsa101
|10.176.102.165 |CASUNNESX105	|--				|# vmhost running alienvault, casunnbo111, casunnbo112, casunnodax101, casunnscan102 (Nessus), Netwok Montitor, OTS108, RSA102

# Operational Documentation #

### Exportinga VM Config ###
1. Login to a Windows box with access to the relevent VM environment 
1. Confirm VMWare PowerCLI is installed. If not get it from the [VMWare Download site](https://my.vmware.com/group/vmware/get-download?downloadGroup=PCLI630R1) (requires login). The file is named ```VMware-PowerCLI-6.3.0-3737840.exe```
1. Open the VSphere PowerCLI
1. Execute the following commands

```powershell
# This will prompt you to enter creds , which will be stored in this temporary variable 
# so you wont have to keep typing them over and over again, unless you are into that sort of thing.
$cred = (get-credential) 

#backup location to Downloads
$backup = "$env:userprofile\Downloads" 

$vmhosts = "sup-ucsesx103","sup-ucsesx104"
Connect-VIServer -Server $vmhosts -credential $cred #This will initiate the connetions to each esx host, apparently you must do this before connecting

# This will create a tarball (.tar) that contains other tarballs when will eventually lead to the file /etc/vmware/esx.conf
Get-VMHostFirmware -VMHost $vmhosts  -BackupConfiguration -DestinationPath $backup 

# Once done go to the downlaads folder and get the esx.conf files 
# This file contains the esx configuration including vm info, network configuration, etc.
```
Store the extracted files ina location for safe keeping. Also you can compare each .conf file with other vmhosts to see changes.
