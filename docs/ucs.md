## UCS Info ##


| Component     | IP            | Notes |
| ------------- |:-------------:| -----:|
| UCS FI-A      | ```10.176.100.162``` | --    |
| UCS FI-B      | ```10.176.100.163``` | --    |
| UCS Mgmt		| ```10.176.100.161``` | --    |
| Nimble iSCSI A| ```10.176.240.10``` | -- |
| Nimble iSCSI B| ```10.176.241.10``` | -- |
| Nimble Mgmt | ```10.176.100.155``` | *This IP floats between both iscsi ip's for redundancy purposes |
| UCS ESXi Host 1 ```SUP-UCSESX101``` | ```10.176.102.151```| -- | 
| UCS ESXi Host 2 ```SUP-UCSESX102``` | ```10.176.102.152```| -- | 
| UCS ESXi Host 3 ```SUP-UCSESX103``` | ```10.176.102.153```| -- | 
| UCS ESXi Host 4 ```SUP-UCSESX104``` | ```10.176.102.154```| -- | 
| New Blade KVM 1 | ```10.176.100.235``` | -- | 
| New Blade KVM 2 | ```10.176.100.236``` | -- | 
| New Blade KVM 3 | ```10.176.100.237``` | -- | 
| New Blade KVM 4 | ```10.176.100.238``` | -- | 
| Nimble Mgmt Portal | [Link](https://10.176.100.155/#home) *on prod vpn | Login in lastpass |
| UCS Mgmt Portal | [HTML](https://10.176.100.161/app/3_1_1k/index.html) *on prod vpn /  [Java](https://10.176.100.161/ucsm/ucsm.jnlp) *on prod vpn | Login in lastpass |
| OVA Template for SQL Load Testing provided by Nimble | [File](https://10.176.102.153/folder/NimbleStorage-Win2K8Server-SQLIO-Generator.ova?dcPath=ha%252ddatacenter&dsName=test%2520lun) | -- |
## UCS Emulator ##
* Can use the exported config from ucs into emulator

## Manually Creating component for service profiles ##

* vnics
* uuid / mac addresses are trandferable with service profile
* kvm management ip 
* service profiles: move profiles from blade 1 to blade 4
* blades are transferable 
* virtual network cards , piped through two 10g links - 20G 
* serivice profiles can have multiple policy profiles
  *  qos for jumbo frames (if we use it)
* san
  * iscsi profiles (iqn)
* network profiles

### Actions ###
1. Equipment > Policies  Port Channel
1. Power policy > Grid 

#### Creatinig pools 
1. Create a UUID Suffix Pool 
* pools cannot be renamed
1. Naming Convention: SUP (Sunnyvale Physical) 
11. prefix = sup-%
11. name: sup-uuid-pool
11. description: blank / optional
11. derices prefix
11. sequential assignment order
1. UUID Blocks
11. standard: 8000 0025b5 01 0000
11. size: 128

#### Creating a LAN Pool
- Create a mac pool
  1. to mac pools to work in conjunction with each fabric for redundancy
  1. sup-mac-pool-a
	1. sequential ordering
	1. block xx:xx:xx::xx:0A:xx
	1. b block sub-mac-pool-b
	1. block => 00:25:B5:01:0B:00 (site number: fabric letter (a/b))

#### Creating IP Pools
  1. blade + iscsi 
  1. ip pools
	1. name: sup-sup-kvm-pool
	1. ordering: sequential
	1. from: 10.176.100.235 - (if wanting to add more we can always add a new pool)
	1. defult gateway: 10.176.100.1 , dns: 10.176.100.111
	1. no ipv6 pools

#### Create iscsi itiiator pool (another ip pool)
1. sup-iscsi-pool-a / b
1. sequential ordering
1. pool a: 10.176.240.50 / size 8 / 10.176.240.1 dg / dns = 10.176.100.111
1. pool b: sup=-iscssi-poo-b / sequencial / 10.76.241.50 / dg=10.176.241.1

#### Crate IQN Pool
1. sub-iqn-pool (no a/b)
1. prefix = iqn.1992.09.com.cisco
1. sequencial ordering
1. suffux = exsihost / from:0 / siez:253

#### Enbaling QOS Classes ( jumo frames are configured here)
Best Effort enabled by default 
1. add a qos policy
11. sup-qos-jumbo / best priootuy / burst byes = default
11. mtu = 9000 (between nimble and ucs only, not network)

#### Network Control Policies
1. sup-cdp-enable / cdp=enabled / mac=only vlan / link down 
1. sup-cdp-disable / cdp=disabled / action on fail = warning

#### Flow Control Policies
1. SUP-nimble-flow
1. priority: auto / recierve:on / send:on

Fab intercon > A  Fixed modules > Ethernet

#### Network Control Policy
1. LAN sup-cdp-disable
1. cdp disabled for nimble
1. VLANS before interfaces
1. Create VLAN (using list docs)
1. VLAN 100 - managment for esxi
1. esx1-mgmt-100 / common global (only iscsi a / b will be decdicated)
1. vlanid = 100
1. vlanname = app-150
1. vlanid = 150
```
iscsi-b-241 - vlan241
# vrf subnets
111 = inet1-2
inet-1 -2 -3
```

#### Create App Interfaces
* fabric a = iscsi-a
* fabric b = iscsi-b

#### Create ESXi VNIC Templatets
1. esxi-mgmt-a / upodating templates / 
1. exsi-mgmt-b
1. Create Native VLAN 999 - Diabled for prevent double tag traffic
  * Untaged traffic gets thrown into 999 and be thrown into black hole of death
1. ESXI-vmotion-a
  * native-999
  * come back in adn add vlan 
1. esxi-vmotion-b
  * fabric b

#### Create ISCSI Templates
* esx-iscsi-a / updating template / make traffic native to icsi to keep traffic , not discard to vlan999 / jumbo frames 
* -b / mac-pool-b / mtu=9000 / qos policy jumbo / 

#### Create LAN Connectivity Policy 
* su-esxi-lan-pool
* Create iscsi adapter profile for nimble

#### Create BIOS Policies
* ```sup-esxi-bios-policies``` - This is the esxi bios policies.  
* reboot on bios settings change 
* turbo boost
* Local Disk Config Policy
  * sup-no-disk
  - no local storage

#### Create maintenance policy
* sub-user-ack
* Scrub Policy 
  1. ```sup-scrub-all``` : policy to 'scrub' or reset the blade policies to wipe all current config and reset to factory
* Host Firmware Package
  1. ```sup-3.0.2c``` : Blade package: 'B' series blaedes / Rack Package: 'c' series
* Boot Policies
  * ```sup-san-boot```  will load iso locall via kvm
  * add iscsi boot ```iscsi b``` then ```iscsi b```

#### Identity service profile template ####
* ```sup-ucsesxp-san-boot```


## Nimble Configuration ##
* iqn address: ```iqn.1992-09.com.cisco:esxihost0```


