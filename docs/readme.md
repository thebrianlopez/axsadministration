# [![Logo][logo]](#) #
1. [Outlook]
1. [Calendar]
1. [PRTG]
1. [PagerDuty]
1. [New Relic]
1. [AWS: axsdevops][AXS AWS Console]
1. [AWS: veritix-west-ec2][Veritix AWS Console]
1. [Ops Issues]
1. [On-sale Calendar](https://axsteam.atlassian.net/wiki/display/DEV/On-Sales+Using+Queue-IT)
---
* [Trello](https://trello.com/b/6d6ErAiQ/engineering-ideas)
* [Slack Channels][#ops-sys-engineering]
	* [#ops-sys-engineering]
	* [#dev-general]
	* [#general]
	* [#qa3]
* [Wiki][Ops Wiki]
	* [Ops Wiki]
	* [Departments Wiki]
	* [Dev Wiki]
* [Issues][Ops Issues]
	* [Ops Issues]
	* [Dev Issues]
	* [Sysops Issues]
* Operations
	* [Veritix AWS Console]
	* [AXS AWS Console]
	* [F5 Loadbalancer](http://10.175.103.231)
	* [PRTG]
	* [New Relic]
	* [Sumologic]
	* [Blue Bridge cloud]
	* [Grafana]
	* [Loggly](https://thebrianlopez.loggly.com/dashboards)
	* [Puppet Dashboard](https://puppet-dashboard.axsops.com/)
	* [Nimble Mgmt](https://10.176.100.155/#administration_network_config)
	* [UCS Mgmt](https://10.176.100.161/app/3_0_2c/index.html)
	* [Last Pass](chrome://lastpass/content/home2.xul)
* CI/Build Infra
	* [Veritix Github]
	* [TeamCity]
	* [Personal AppVeyor]
	* [Octopus Deploy]
	* [Personal MyGet]
	* [Veritix MyGet]
	* [AXS Github](https://github.com/aegaxs/)
	* [Rundeck](https://rundeck.axsops.com/user/login)
	* [TeamCity]
	* [Octopus Deploy]
	* [Check Deployment Status]
	* Jenkins
		* http://jenkins.axsops.com:8080/
		* http://build.axsops.com/
		* https://jenkins-new.axsops.com/
	* [Upsource](https://upsource.axsops.com/)
	* [Icinga2](https://monitor.axsops.com/)
---
* Snippets
	* Get top 10 Processcess sorted my physical memory
```powershell
Get-Process -computername applog101 | sort PM -descending | select -first 10
```
	* Get number of logged users
```powershell
quser /server:localhost
```
	* Get Service Status
```powershell
Get-WmiObject -ComputerName ots109 -Query "select * from win32_service where Name like 'w3svc'"
```
	* Get Active Directory Computer Objects (Requires ActiveDirectory PowerShell Module)
```
Get-ADComputer -Server corp-dc101 -Filter '*' | select DNSHostName | where DNSHostName -like '*corp-*'
```
	* Install Git Portable package (does not require admin rights or installation)
```
[uri]$a = "https://github.com/git-for-windows/git/releases/download/v2.10.0.windows.1/PortableGit-2.10.0-32-bit.7z.exe"
$b="$env:userprofile\Downloads\$([io.path]::GetFileName($a.LocalPath))"
(new-object net.webclient).DownloadFile($a,$b) | iex
```
	* Disable SSL cert checking during an existing PowerShell Session and get page data
```
[Net.ServicePointManager]::ServerCertificateValidationCallback = {$true}
"https://monitor.veritix.net/api/public/testlogin.htm" | %{(New-Object net.webclient).DownloadString($_)}
```
	* Linux: Get System Make and Model
```
dmidecode | grep -A3 '^System Information' 
```
	* Chef Knife ec2 delete and purge a node from both ec2 and chef server
```bash
knife ec2 server delete --node-name i-09432fe652bd63c7c --purge --yes --verbose
```
	* List running docker instances and get imageids only (useful for killing multiple instances at once)
```
(docker ps | awk {'print $3'})
```
	* EventISs for Service Control Manager notifications , [Microsoft Reference](https://technet.microsoft.com/en-us/library/dd349427(v=ws.10).aspx)
```
  Service Start = 
```

[Ops Issues]: https://axsteam.atlassian.net/secure/RapidBoard.jspa?projectKey=OPS&rapidView=78&view=planning
[SysOps Issues]: https://axsteam.atlassian.net/projects/SYSOPS/issues?&orderby=created+DESC
[Dev Issues]: https://axsteam.atlassian.net/projects/DEVTEAM/issues?&orderby=created+DESC
[Wiki]: https://axsteam.atlassian.net/wiki/discover/all-updates
[Dev Wiki]: https://axsteam.atlassian.net/wiki/display/DEV/
[Ops Wiki]: https://axsteam.atlassian.net/wiki/display/OPS/
[Departments Wiki]: https://axsteam.atlassian.net/wiki/display/FTD/
[#qa3]: https://axsall.slack.com/messages/qa3/
[#dev-general]: https://axsall.slack.com/messages/dev-general/
[#general]: https://axsall.slack.com/messages/general/
[#sysops-private-cle]: https://axsall.slack.com/messages/sysops-private-cle/
[#sysops-private-dal]: https://axsall.slack.com/messages/sysops-private-dal/
[#sysops-private-la]: https://axsall.slack.com/messages/sysops-private-la/
[#ops-sys-engineering]: https://axsall.slack.com/messages/ops-sys-engineering/
[#ops-private-chat]: https://axsall.slack.com/messages/ops-private-chat/
[prtg]: https://monitor.veritix.net/sensors.htm?id=0&filter_status=5
[new relic]: https://rpm.newrelic.com/accounts/686338/applications
[logo]: https://axsteam.atlassian.net/wiki/download/attachments/655361/global.logo?version=1&modificationDate=1464206702147&api=v2
[Veritix Github]: https://github.com/orgs/Veritix/dashboard
[Personal MyGet]: https://www.myget.org/feed/Details/thebrianlopez
[AXS Github]: https://github.com/aegaxs/ 
[Veritix AWS Console]: https://veritix-west-ec2.signin.aws.amazon.com/console
[AXS AWS Console]: https://axsdevops.signin.aws.amazon.com/console
[blue bridge cloud]: https://cloud.bluebridgenetworks.com/
[OpsServer]: http://52.52.79.77/hub
[TeamCity]: http://teamcity.corp.veritix.com:8081/
[Octopus Deploy]: http://octopus.corp.veritix.com/
[Veritix MyGet]: https://www.myget.org/F/veritix/
[Grafana]: https://graphs.axsops.com/grafana/login
[Sumologic]: https://service.us2.sumologic.com/ui/?goto=welcomeAfterPwdChange#section/live_tail
[Todoist]: https://todoist.com/app
[Outlook]: https://outlook.office.com/owa/?path=/mail/inbox
[Calendar]: https://outlook.office.com/owa/?path=/calendar/view/Week
[Personal AppVeyor]: https://ci.appveyor.com/project/thebrianlopez/axsadministration
[Trello]: https://trello.com/b/6d6ErAiQ/engineering-ideas
[Onsale]: https://axsteam.atlassian.net/wiki/display/DEV/On-Sales+Using+Queue-IT
[PagerDuty]: http://outbox.pagerduty.com/
[Sumologic]: https://service.us2.sumologic.com/ui/dashboard.html?f=23640125&t=r
