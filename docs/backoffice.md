# Backoffice #
* [Link to PRTG Group](https://monitor.veritix.net/group.htm?id=2718&tabid=1)

| name | vmhost | servername | ip address | 
|----- | :------: |:----------:| ----------:|
| -- | -- | ```bo101.na.veritix.net``` | -- |
| -- | -- | ```bo102.na.veritix.net``` | -- |
| -- | -- | ```bo103.na.veritix.net``` | -- |
| -- | -- | ```bo104.na.veritix.net``` | -- |
| -- | -- | ```bo105.na.veritix.net``` | -- |
| -- | -- | ```bo106.na.veritix.net``` | -- |
| -- | -- | ```bo107.na.veritix.net``` | -- |
| -- | -- | ```bo108.na.veritix.net``` | -- |
| -- | -- | ```CASUNNBO109.na.veritix.net``` | -- |
| -- | -- | ```CASUNNBO110.na.veritix.net``` | -- |
| -- | -- | ```CASUNNBO111.na.veritix.net``` | -- |
| -- | -- | ```CASUNNBO112.na.veritix.net``` | -- |
| -- | -- | ```CASUNNBO113.na.veritix.net``` | -- |
| -- | -- | ```CASUNNBO114.na.veritix.net``` | -- |
| -- | -- | ```CASUNNBO115.na.veritix.net``` | -- |

